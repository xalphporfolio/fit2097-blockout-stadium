// Fill out your copyright notice in the Description page of Project Settings.


#include "SAKick.h"
#include "Ball.h"
#include "Lab_BlockoutStadiumCharacter.h"
#include "NiagaraFunctionLibrary.h"
#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"


USAKick::USAKick()
{
	PrimaryComponentTick.bCanEverTick = false;

	KickDistanceOffset = 200.0f;
	KickPower = 1500.0f;
}


void USAKick::Kick()
{
	ServerKick();
}

void USAKick::ServerKick_Implementation()
{
	MulticastKick();
	
	TArray<FHitResult> HitResults;
	const FVector Start = GetComponentLocation();
	const FVector End = Start + GetForwardVector() * KickDistanceOffset;


	const FCollisionShape CubeShape = FCollisionShape::MakeBox(FVector(KickDistanceOffset));
	GetWorld()->SweepMultiByChannel(HitResults, End, End, GetComponentQuat(), ECC_WorldDynamic, CubeShape);

	for (auto& HitResult : HitResults)
	{
		if (HitResult.GetActor() != GetOwner())
		{
			UStaticMeshComponent* HitMesh = Cast<UStaticMeshComponent>(HitResult.GetActor()->GetRootComponent());
			if (!HitMesh)
				return;

			HitMesh->AddRadialImpulse(Start, KickDistanceOffset * 2, KickPower, ERadialImpulseFalloff::RIF_Linear, true);

			if (Cast<ABall>(HitResult.GetActor()) && Cast<ALab_BlockoutStadiumCharacter>(GetOwner()))
			{
				Cast<ABall>(HitResult.GetActor())->SetLastPlayer(Cast<ALab_BlockoutStadiumCharacter>(GetOwner()));

				MulticastKick(true);
			}
		}
	}
}

void USAKick::MulticastKick_Implementation(bool bHasHit)
{
	if (bHasHit)
	{
		if (Cast<APawn>(GetOwner()) && Cast<APawn>(GetOwner())->IsLocallyControlled())
		{
			PlayHitSound();
		}
		
		OnKickHit.Broadcast();
	}
	else
	{
		const FVector Start = GetComponentLocation();
		const FVector End = Start + GetForwardVector() * KickDistanceOffset;
	
		if (NSKick)
		{
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), NSKick, End, GetComponentRotation());
		}

		if (Cast<APawn>(GetOwner()) && Cast<APawn>(GetOwner())->IsLocallyControlled())
		{
			PlayKickSound();
		}

		OnKick.Broadcast(End);
	}
}


void USAKick::PlayKickSound()
{
	if (GetOwner()->HasNetOwner())
		ServerPlayKickSound();
}

void USAKick::ServerPlayKickSound_Implementation()
{
	MulticastPlayKickSound();
}

void USAKick::MulticastPlayKickSound_Implementation()
{
	if (SBKick)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SBKick, GetComponentLocation());
	}
}


void USAKick::PlayHitSound()
{
	if (GetOwner()->HasNetOwner())
		ServerPlayHitSound();
}


void USAKick::ServerPlayHitSound_Implementation()
{
	MulticastPlayKickSound();
}

void USAKick::MulticastPlayHitSound_Implementation()
{
	if (SBHit)
	{
		float RemapVel =  FMath::Lerp(0.2f, 1.0f,
			UKismetMathLibrary::NormalizeToRange(KickPower, 0.0f, 300.0f));

		RemapVel = FMath::Clamp(RemapVel, 0.0f, 1.0f);
		
		UAudioComponent* SBAudio = UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SBHit, GetComponentLocation());
		SBAudio->SetFloatParameter(FName("CollisionVelNorm"), RemapVel);
		SBAudio->SetTriggerParameter(FName("On Collide"));
	}
}
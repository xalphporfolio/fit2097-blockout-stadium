// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "Field/FieldSystemActor.h"
#include "SlimeAcidPatch.generated.h"


UCLASS()
class LAB_BLOCKOUTSTADIUM_API ASlimeAcidPatch : public AFieldSystemActor
{
	GENERATED_BODY()

	
public:
	ASlimeAcidPatch();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UDecalComponent* PatchDecal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USphereComponent* SphereCollision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UReturnResultsTerminal* ReturnResultsTerminal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	URadialFalloff* RadialFalloff;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UUniformScalar* UniformScalar;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UOperatorField* OperatorField;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCullingField* CullingField;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DefaultCollisionRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector DefaultDecalSize;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* DecalMaterialClass;
	UMaterialInstanceDynamic* DecalMaterialInstance;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* SBOnPlace;

	
protected:
	FTimerHandle AcidDurationTimerHandle;

	UPROPERTY(Replicated)
	float AcidDamage;
	

public:
	virtual void Tick(float DeltaSeconds) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	void SetAcidPatch(const float Scale, const FLinearColor Colour, const float Rotation, const float AcidDuration,
		const float AcidTickDamage);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetAcidPatch(const float Scale, const FLinearColor Colour, const float Rotation, const float AcidDuration,
		const float AcidTickDamage);

	void TickDamage();
	UFUNCTION(Server, Reliable)
	void ServerTickDamage();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastTickDamage();
	
	void SetDecal(float Scale, FLinearColor Colour, float DecalRotation);
	UFUNCTION(Server, Unreliable)
	void ServerSetDecal(float Scale, FLinearColor Colour, float DecalRotation);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastSetDecal(float Scale, FLinearColor Colour, float DecalRotation);

	void PlaySound(float Scale);
	UFUNCTION(Server, Unreliable)
	void ServerPlaySound(float Scale);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastPlaySound(float Scale);

	
protected:
	virtual void BeginPlay() override;
	
	void OnAcidDurationTimer();
};

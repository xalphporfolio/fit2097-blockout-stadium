// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SASizeModifier.generated.h"


UENUM(BlueprintType)
enum class EActorSize:uint8
{
	SIZE_LARGE UMETA(DisplayName = "Large"),
	SIZE_MID UMETA(DisplayName = "Medium"),
	SIZE_SMALL UMETA(DisplayName = "Small"),
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSizeChanged, const EActorSize, NewSize);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class LAB_BLOCKOUTSTADIUM_API USASizeModifier : public UActorComponent
{
	GENERATED_BODY()

public:
	FOnSizeChanged OnSizeChanged;
	
	UPROPERTY(Replicated)
	EActorSize CurrentSize;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, EditFixedSize, meta = (EditFixedOrder))
	TArray<FVector> SizeScales;


protected:
	virtual void BeginPlay() override;
	
	
public:
	USASizeModifier();
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void SetSize(const EActorSize NewSize);
	UFUNCTION(Server, Reliable)
	void ServerSetSize(const EActorSize NewSize);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastSetSize(const EActorSize NewSize);
	
	void ReduceSize();
	void IncreaseSize();

	EActorSize GetCurrentSize() const;
	FVector GetCurrentScale() const;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NiagaraComponent.h"
#include "Components/ActorComponent.h"
#include "SASizeModifier.h"
#include "SAJumpLaunch.generated.h"

class ALab_BlockoutStadiumCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChargeStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChargeEnd);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChargeMax);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharacterJump);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharacterLaunched);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class LAB_BLOCKOUTSTADIUM_API USAJumpLaunch : public UActorComponent
{
	GENERATED_BODY()

private:
	UPROPERTY()
	ALab_BlockoutStadiumCharacter* PlayerRef;
	UPROPERTY()
	USASizeModifier* SizeModifierCompRef;
	
	UPROPERTY(Replicated)
	bool bIsCharging;
	UPROPERTY(Replicated)
	float CurrentChargeTime;
	
	
public:	
	FOnChargeStart OnChargeStart;
	FOnChargeEnd OnChargeEnd;
	FOnChargeMax OnChargeMax;
	FOnCharacterJump OnCharacterJump; 
	FOnCharacterLaunched OnCharacterLaunched;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, EditFixedSize, meta = (EditFixedOrder))
	TArray<float> JumpSpeeds;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, EditFixedSize, meta = (EditFixedOrder))
	TArray<float> LaunchForces;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float MinChargeTime;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float MaxChargeTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FRotator LaunchAngleOffset;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float MaxLaunchPitch;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UNiagaraSystem* NSCharge;
	
	UPROPERTY()
	UNiagaraComponent* ChargeParticles;

	UPROPERTY()
	UAudioComponent* ChargeAudioComponent;
	bool bIsChargeSoundsPlaying;
	bool bIsMaxChargeSoundsPlaying;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* SBCharge;
	UPROPERTY(EditDefaultsOnly)
	USoundBase* SBLaunch;

	
protected:
	UFUNCTION()
	void OnSizeChanged(const EActorSize NewSize);

	
public:
	USAJumpLaunch();

	void InitComponent(ALab_BlockoutStadiumCharacter* Player, USASizeModifier* SizeModifier);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	bool IsCharging() const;

	void LaunchStart();
	UFUNCTION(Server, Reliable)	
	void ServerLaunchStart();

	void LaunchEnd();
	UFUNCTION(Server, Reliable)	
	void ServerLaunchEnd();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastLaunchEnd();

	void CancelCharge();
	UFUNCTION(Server, Reliable)	
	void ServerCancelCharge();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastCancelCharge();

	void ChargingParticles(bool IsMaxCharge);
	UFUNCTION(Server, Unreliable)	
	void ServerChargingParticles(bool IsMaxCharge);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastChargingParticles(bool IsMaxCharge);

	void StartChargeSounds(bool bIsMaxCharge);
	UFUNCTION(Server, Unreliable)
	void ServerStartChargeSounds(bool bIsMaxCharge);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastStartChargeSounds(bool bIsMaxCharge);

	void StopChargeSounds();
	UFUNCTION(Server, Unreliable)
	void ServerStopChargeSounds();
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastStopChargeSounds();

	void PlayLaunchSound();
	UFUNCTION(Server, Unreliable)
	void ServerPlayLaunchSound();
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastPlayLaunchSound();
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHUDWidget.h"

#include "Components/CanvasPanel.h"


void UPlayerHUDWidget::NativeConstruct()
{
	Super::NativeConstruct();

	GameState = Cast<ASportGameState>(GetWorld()->GetGameState());

	if (GameState)
	{
		GameState->OnGameStart.AddDynamic(this, &UPlayerHUDWidget::OnGameStart);
	}
}


void UPlayerHUDWidget::OnGameStart()
{
	WaitingScreen->SetVisibility(ESlateVisibility::Hidden);
}

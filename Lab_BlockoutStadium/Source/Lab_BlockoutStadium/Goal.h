// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "SportGameState.h"
#include "NiagaraComponent.h"
#include "SASizeModifier.h"
#include "Components/RectLightComponent.h"
#include "Net/UnrealNetwork.h"
#include "Goal.generated.h"


UCLASS()
class LAB_BLOCKOUTSTADIUM_API AGoal : public AActor
{
	GENERATED_BODY()


protected:
	ASportGameState* GameState;
	
	
public:	
	AGoal();

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* GoalMesh;

	UPROPERTY(EditDefaultsOnly)
	URectLightComponent* InnerGoalLight;

	UPROPERTY(EditDefaultsOnly)
	UBoxComponent* GoalCollider;

	UPROPERTY(EditDefaultsOnly)
	UNiagaraSystem* NS_GoalExplosion;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* MaterialClass;
	UMaterialInstanceDynamic* MaterialInstance;
	
	UPROPERTY(EditAnywhere, Replicated)
	EPlayerTeam Team;

	UPROPERTY(EditDefaultsOnly)
	TArray<int> SizePoints;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* SBGoal;

	
protected:
	virtual void BeginPlay() override;

	int GetPoints(EActorSize ScorerSize);

	
public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnGameEnd();
	
	void ScoreGoal(int Points);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerScoreGoal(int Points);

	void PlayGoalSound();
	UFUNCTION(Server, Unreliable)
	void ServerPlayGoalSound();
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastPlayGoalSound();

	void ChangeColour(EPlayerTeam CurrentTeam);
	UFUNCTION(Server, Unreliable)
	void ServerChangeColour(EPlayerTeam CurrentTeam);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastChangeColour(EPlayerTeam CurrentTeam);
	
	UFUNCTION(Server, Unreliable)
	void ServerSpawnGoalParticles(FVector SpawnLocation);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastSpawnGoalParticles(FVector SpawnLocation);
};
 

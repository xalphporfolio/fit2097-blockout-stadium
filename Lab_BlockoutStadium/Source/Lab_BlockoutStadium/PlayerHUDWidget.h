// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ScoreWidget.h"
#include "Blueprint/UserWidget.h"
#include "PlayerHUDWidget.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class LAB_BLOCKOUTSTADIUM_API UPlayerHUDWidget : public UUserWidget
{
	GENERATED_BODY()


protected:
	ASportGameState* GameState;
	
	UPROPERTY(meta = (BindWidget))
	class UScoreWidget* ScoreWidget;

	UPROPERTY(meta = (BindWidget))
	class URoundTimerWidget* RoundTimerWidget;

	UPROPERTY(meta = (BindWidget))
	class UCanvasPanel* WaitingScreen;


protected:
	UFUNCTION()
	void OnGameStart();
	
	
public:
	virtual void NativeConstruct() override;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "Goal.h"

#include "Ball.h"
#include "Lab_BlockoutStadiumCharacter.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"


AGoal::AGoal()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	GoalMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Goal Mesh"));
	GoalMesh->SetupAttachment(RootComponent);

	InnerGoalLight = CreateDefaultSubobject<URectLightComponent>(TEXT("Inner Goal Light"));
	InnerGoalLight->SetupAttachment(GoalMesh);

	GoalCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Component"));
	GoalCollider->SetupAttachment(GoalMesh);

	Team = EPlayerTeam::TEAM_NONE;

	SizePoints = {3, 2, 1};
}


void AGoal::BeginPlay()
{
	Super::BeginPlay();

	GameState = Cast<ASportGameState>(GetWorld()->GetGameState());

	if (GameState)
	{
		GameState->OnGameEnd.AddDynamic(this, &AGoal::OnGameEnd);
	}
	
	GoalCollider->OnComponentBeginOverlap.AddDynamic(this, &AGoal::OnBeginOverlap);

	if (MaterialClass)
	{
		MaterialInstance = UMaterialInstanceDynamic::Create(MaterialClass, this);
		GoalMesh->SetMaterial(0, MaterialInstance);
	}

	MulticastChangeColour(Team);
}


int AGoal::GetPoints(EActorSize ScorerSize)
{
	return SizePoints[static_cast<int>(ScorerSize)];
}


void AGoal::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGoal, Team);
}


void AGoal::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                           UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!HasAuthority())
		return;
	
	if (OtherActor && OtherActor != this)
	{
		// UE_LOG(LogTemp, Warning, TEXT("Goal overlapped with something"));

		if (Cast<ABall>(OtherActor))
		{
			// UE_LOG(LogTemp, Warning, TEXT("GOOOOOOOAL!"));

			const ABall* Ball = Cast<ABall>(OtherActor);
			if (Ball->LastPlayer && Ball->LastPlayer->GetCurrentTeam() == Team)
			{
				if (GameState)
				{
					ScoreGoal(GetPoints(Ball->LastPlayer->SizeModifierComponent->GetCurrentSize()));
					ServerSpawnGoalParticles(Ball->GetActorLocation());

					if (Ball->LastPlayer->GetPlayerState())
					{
						if (Cast<ASportPlayerState>(Ball->LastPlayer->GetPlayerState()))
						{
							Cast<ASportPlayerState>(Ball->LastPlayer->GetPlayerState())->AddGoals(1);
						}
					}

					GameState->GoalScored();
					PlayGoalSound();
				}
			}
			else
			{
				// UE_LOG(LogTemp, Warning, TEXT("Ball had no last player or hit the wrong team's goal"));
			}
		}
		else
		{
			// UE_LOG(LogTemp, Warning, TEXT("This isn't a ball"));	
		}
	}
}


void AGoal::OnGameEnd()
{
	GoalCollider->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}


void AGoal::ScoreGoal(int Points)
{
	ServerScoreGoal(Points);
}

bool AGoal::ServerScoreGoal_Validate(int Points)
{
	if (Points < 1 || Points > 3)
		return false;
	
	return true;
}

void AGoal::ServerScoreGoal_Implementation(int Points)
{
	switch (Team)
	{
	case EPlayerTeam::TEAM1:
		GameState->TeamOneGoalScored(Points);
		break;

	case EPlayerTeam::TEAM2:
		GameState->TeamTwoGoalScored(Points);
		break;

	default:
		break;
	}
}


void AGoal::PlayGoalSound()
{
	ServerPlayGoalSound();
}

void AGoal::ServerPlayGoalSound_Implementation()
{
	MulticastPlayGoalSound();
}

void AGoal::MulticastPlayGoalSound_Implementation()
{
	if (SBGoal)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), SBGoal);
	}
}


void AGoal::ChangeColour(EPlayerTeam CurrentTeam)
{
	ServerChangeColour(CurrentTeam);
}


void AGoal::ServerChangeColour_Implementation(EPlayerTeam CurrentTeam)
{
	Team = CurrentTeam;
	
	MulticastChangeColour(CurrentTeam);

	// UE_LOG(LogTemp, Warning, TEXT("Goal Colour Changed (Server RPC): %s"), *UEnum::GetValueAsString(CurrentTeam));
}

void AGoal::MulticastChangeColour_Implementation(EPlayerTeam CurrentTeam)
{
	if (!MaterialInstance)
		return;
	
	switch (CurrentTeam)
	{
	case EPlayerTeam::TEAM_NONE:
		MaterialInstance->SetVectorParameterValue("Colour", GameState->TeamNoneColour);
		InnerGoalLight->SetLightColor(GameState->TeamNoneColour);
		break;

	case EPlayerTeam::TEAM1:
		MaterialInstance->SetVectorParameterValue("Colour", GameState->TeamOneColour);
		InnerGoalLight->SetLightColor(GameState->TeamOneColour);
		break;

	case EPlayerTeam::TEAM2:
		MaterialInstance->SetVectorParameterValue("Colour", GameState->TeamTwoColour);
		InnerGoalLight->SetLightColor(GameState->TeamTwoColour);
		break;

	default:
		break;
	}

	// UE_LOG(LogTemp, Warning, TEXT("Goal Colour Changed (Multicast RPC): %s"), *UEnum::GetValueAsString(CurrentTeam));
}


void AGoal::ServerSpawnGoalParticles_Implementation(FVector SpawnLocation)
{
	MulticastSpawnGoalParticles(SpawnLocation);
}

void AGoal::MulticastSpawnGoalParticles_Implementation(FVector SpawnLocation)
{
	if (NS_GoalExplosion)
	{
		UNiagaraComponent* GoalExplosion = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),
			NS_GoalExplosion, SpawnLocation, GetActorForwardVector().Rotation());

		switch (Team)
		{
		case EPlayerTeam::TEAM_NONE:
			GoalExplosion->SetNiagaraVariableLinearColor(FString("User.Colour"), GameState->TeamNoneColour);
			break;
			
		case EPlayerTeam::TEAM1:
			GoalExplosion->SetNiagaraVariableLinearColor(FString("User.Colour"), GameState->TeamOneColour);
			break;

		case EPlayerTeam::TEAM2:
			GoalExplosion->SetNiagaraVariableLinearColor(FString("User.Colour"), GameState->TeamTwoColour);
			break;

		default:
			break;
		}
	}
}

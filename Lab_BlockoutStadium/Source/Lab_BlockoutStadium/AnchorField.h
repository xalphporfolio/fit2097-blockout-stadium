// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "Field/FieldSystemActor.h"
#include "AnchorField.generated.h"


UCLASS()
class LAB_BLOCKOUTSTADIUM_API AAnchorField : public AFieldSystemActor
{
	GENERATED_BODY()


public:
	AAnchorField();

	UPROPERTY(EditAnywhere)
	UBoxComponent* BoxCollision;

	UPROPERTY(EditAnywhere)
	UBoxFalloff* BoxFalloff;

	UPROPERTY(EditAnywhere)
	UUniformInteger* UniformInteger;

	UPROPERTY(EditAnywhere)
	UCullingField* CullingField;


public:
	virtual void OnConstruction(const FTransform& Transform) override;
};

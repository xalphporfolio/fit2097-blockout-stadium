// Fill out your copyright notice in the Description page of Project Settings.


#include "SportGameState.h"

#include "Ball.h"
#include "Lab_BlockoutStadiumCharacter.h"
#include "Kismet/GameplayStatics.h"


ASportGameState::ASportGameState()
{
	PrimaryActorTick.bCanEverTick = true;

	bIsMultiplayer = true;
	
	bGameStarted = false;
	RoundTimeSeconds = 30.0f;

	ScoreDelayTime = 2.0f;
	
	TeamOneScore = 0;
	TeamTwoScore = 0;

	TeamNoneColour = FLinearColor::Gray;
	TeamOneColour = FLinearColor::Red;
	TeamTwoColour = FLinearColor::Blue;
}


void ASportGameState::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> FoundActors;
	
	// get team one spawn pos and rot
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "TeamOneSpawnPos", FoundActors);

	if (FoundActors.Num() > 0)
	{
		TeamOneSpawnPos = FoundActors[0]->GetActorLocation();
		TeamOneSpawnRot = FoundActors[0]->GetActorRotation();
	}

	FoundActors.Empty();
	
	// get team two spawn pos and rot
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "TeamTwoSpawnPos", FoundActors);

	if (FoundActors.Num() > 0)
	{
		TeamTwoSpawnPos = FoundActors[0]->GetActorLocation();
		TeamTwoSpawnRot = FoundActors[0]->GetActorRotation();
	}

	// get ball spawn pos and rot
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "BallSpawnPos", FoundActors);

	if (FoundActors.Num() > 0)
	{
		BallSpawnPos = FoundActors[0]->GetActorLocation();
		BallSpawnRot = FoundActors[0]->GetActorRotation();
	}

	// UE_LOG(LogTemp, Warning, TEXT("Sport State begin play"));
	// UE_LOG(LogTemp, Warning, TEXT("Spawn1 Pos: %s| Spawn1 Rot: %s"), *TeamOneSpawnPos.ToString(), *TeamOneSpawnRot.ToString());
	// UE_LOG(LogTemp, Warning, TEXT("Spawn2 Pos: %s| Spawn2 Rot: %s"), *TeamTwoSpawnPos.ToString(), *TeamTwoSpawnRot.ToString());
}


void ASportGameState::OnBallDestroyed(AActor* DestroyedActor)
{
	if (!HasAuthority())
		return;
	
	if (CurrentBall == DestroyedActor)
	{
		CurrentBall->OnDestroyed.RemoveDynamic(this, &ASportGameState::OnBallDestroyed);

		SpawnNewBall();
	}
	else
	{
		// UE_LOG(LogTemp, Warning, TEXT("There is no CurrentBall reference yet the destroyed callback was run"));
	}
}


void ASportGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASportGameState, bGameStarted);
	DOREPLIFETIME(ASportGameState, CurrentRoundTimeRemaining);

	DOREPLIFETIME(ASportGameState, TeamOne);
	DOREPLIFETIME(ASportGameState, TeamTwo);
	
	DOREPLIFETIME(ASportGameState, TeamOneScore);
	DOREPLIFETIME(ASportGameState, TeamTwoScore);

	DOREPLIFETIME(ASportGameState, CurrentBall);
}


void ASportGameState::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (HasAuthority())
	{
		if (bGameStarted)
		{
			CurrentRoundTimeRemaining = GetWorld()->GetTimerManager().GetTimerRemaining(GameTimerHandle);
		}
		else
		{
			CurrentRoundTimeRemaining = 0.0f;
		}
	}
}


void ASportGameState::AddPlayerState(APlayerState* PlayerState)
{
	Super::AddPlayerState(PlayerState);

	if (!HasAuthority())
		return;
	
	if (Cast<ASportPlayerState>(PlayerState))
	{
		ASportPlayerState* NewSportPlayerState = Cast<ASportPlayerState>(PlayerState);

		if (TeamOne.Contains(NewSportPlayerState) || TeamTwo.Contains(NewSportPlayerState))
			return;

		if (TeamOne.Num() < TeamTwo.Num())
		{
			TeamOne.Add(NewSportPlayerState);
		}
		else if (TeamOne.Num() > TeamTwo.Num())
		{
			TeamTwo.Add(NewSportPlayerState);
		}
		else
		{
			if (rand() % 2 == 0)
			{
				TeamOne.Add(NewSportPlayerState);
			}
			else
			{
				TeamTwo.Add(NewSportPlayerState);
			}
		}

		// UE_LOG(LogTemp, Warning, TEXT("Sport State Add Player State"));

		if (bIsMultiplayer && HasAuthority())
		{
			// start game when there is a player on each team
			if (TeamOne.Num() > 0 && TeamTwo.Num() > 0 && !bGameStarted)
			{
				if (HasAuthority() && !GetWorld()->GetTimerManager().IsTimerActive(GameStartDelayHandle))
				{
					GetWorld()->GetTimerManager().SetTimer(GameStartDelayHandle, this, &ASportGameState::OnGameStartDelay, 0.5f, false);
				}
			}	
		}
		else
		{
			if (!bGameStarted)
				GetWorld()->GetTimerManager().SetTimer(GameStartDelayHandle, this, &ASportGameState::OnGameStartDelay, 0.5f, false);
		}
	}
}


void ASportGameState::SetUpPlayer(APlayerState* PlayerState)
{
	ServerSetUpPlayer(PlayerState);
}

void ASportGameState::ServerSetUpPlayer_Implementation(APlayerState* PlayerState)
{
	if (!PlayerState || !PlayerState->GetPlayerController())
	{
		// UE_LOG(LogTemp, Warning, TEXT("No Valid PlayerState or PlayerController"));
		return;
	}
	
	if (!PlayerState->GetPlayerController()->GetPawn())
	{
		ALab_BlockoutStadiumCharacter* SpawnedPlayer = SpawnNewPlayer(GetPlayerTeam(PlayerState));
		
		PlayerState->GetPlayerController()->Possess(SpawnedPlayer);
		// UE_LOG(LogTemp, Warning, TEXT("Possession should be set"));

		MulticastSetUpPlayer(PlayerState);

		if (Cast<ASportPlayerState>(PlayerState))
		{
			// Note: need to change to actual usernames in the future (11/10/2022)
			Cast<ASportPlayerState>(PlayerState)->SetFieldName(PlayerState->GetPlayerController()->GetActorLabel());
		}
	}
}

void ASportGameState::MulticastSetUpPlayer_Implementation(APlayerState* PlayerState)
{
	if (!PlayerState || !PlayerState->GetPlayerController())
	{
		// UE_LOG(LogTemp, Warning, TEXT("No Valid PlayerState or PlayerController"));
		return;
	}
	
	// set rotation of pawn
	if (GetPlayerTeam(PlayerState) == EPlayerTeam::TEAM1)
	{
		PlayerState->GetPawn()->SetActorRotation(TeamOneSpawnRot);
		PlayerState->GetPlayerController()->SetControlRotation(TeamOneSpawnRot);
	}
	else
	{
		PlayerState->GetPawn()->SetActorRotation(TeamTwoSpawnRot);
		PlayerState->GetPlayerController()->SetControlRotation(TeamTwoSpawnRot);
	}
}


ALab_BlockoutStadiumCharacter* ASportGameState::SpawnNewPlayer(const EPlayerTeam TargetTeam) const
{
	FRotator SpawnRotation;
	FVector SpawnLocation;

	if (TargetTeam == EPlayerTeam::TEAM1)
	{
		SpawnRotation = TeamOneSpawnRot;
		SpawnLocation = TeamOneSpawnPos;
	}
	else
	{
		SpawnRotation = TeamTwoSpawnRot;
		SpawnLocation = TeamTwoSpawnPos;
	}
	
	FActorSpawnParameters ActorSpawnParameters;
	ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	return GetWorld()->SpawnActor<ALab_BlockoutStadiumCharacter>(PlayerCharacter, SpawnLocation, SpawnRotation, ActorSpawnParameters);
}


EPlayerTeam ASportGameState::GetPlayerTeam(APlayerState* PlayerState) const
{
	if (TeamOne.Contains(PlayerState))
	{
		return EPlayerTeam::TEAM1;
	}
	else if (TeamTwo.Contains(PlayerState))
	{
		return EPlayerTeam::TEAM2;
	}
	else
	{
		return EPlayerTeam::TEAM_NONE;
	}
}


ABall* ASportGameState::SpawnNewBall()
{
	FActorSpawnParameters ActorSpawnParameters;
	ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	ABall* SpawnedBall = GetWorld()->SpawnActor<ABall>(BallClass, BallSpawnPos, BallSpawnRot, ActorSpawnParameters);

	CurrentBall = SpawnedBall;
	
	SpawnedBall->OnDestroyed.AddDynamic(this, &ASportGameState::OnBallDestroyed);
	
	return SpawnedBall; 
}


void ASportGameState::StartGame()
{
	ServerStartGame();		
}

void ASportGameState::ServerStartGame_Implementation()
{
	if (!bGameStarted)
	{
		bGameStarted = true;
		GetWorld()->GetTimerManager().SetTimer(GameTimerHandle, this, &ASportGameState::OnGameTimerEnd, RoundTimeSeconds, false);

		SpawnNewBall();
		
		OnGameStart.Broadcast();	
	}
}

void ASportGameState::OnGameTimerEnd()
{
	StopGame();
}


void ASportGameState::OnGameStartDelay()
{
	GetWorld()->GetTimerManager().ClearTimer(GameStartDelayHandle);
	
	if (!bGameStarted)
	{
		StartGame();
	}
}


void ASportGameState::GoalScored()
{
	ServerGoalScored();
}

void ASportGameState::ServerGoalScored_Implementation()
{
	GetWorld()->GetTimerManager().PauseTimer(GameTimerHandle);
	GetWorld()->GetTimerManager().SetTimer(GameScoreDelayHandle, this, &ASportGameState::ResetRound, ScoreDelayTime, false);

	CurrentBall->OnDestroyed.RemoveDynamic(this, &ASportGameState::OnBallDestroyed);
	CurrentBall->Destroy();
}


void ASportGameState::OnGameScoreDelay()
{
	GetWorld()->GetTimerManager().ClearTimer(GameTimerHandle);

	ResetRound();
}


void ASportGameState::ResetRound()
{
	ServerResetRound();
}

void ASportGameState::ServerResetRound_Implementation()
{
	// remove all play characters
	OnRoundReset.Broadcast();

	SpawnNewBall();

	GetWorld()->GetTimerManager().UnPauseTimer(GameTimerHandle);
}


void ASportGameState::StopGame()
{
	ServerStopGame();
}

void ASportGameState::ServerStopGame_Implementation()
{
	if (bGameStarted)
	{
		bGameStarted = false;
		GetWorld()->GetTimerManager().ClearTimer(GameTimerHandle);

		OnGameEnd.Broadcast();
	}
}


void ASportGameState::OnRep_GameStarted()
{
	if (bGameStarted)
	{
		OnGameStart.Broadcast();	
	}
	else
	{
		OnGameEnd.Broadcast();
	}

	// UE_LOG(LogTemp, Warning, TEXT("Current Game Boolean: %s"), ( bGameStarted ? TEXT("true") : TEXT("false")));
}


void ASportGameState::TeamOneGoalScored(int Amount)
{
	ServerTeamOneGoalScored(Amount);
}

bool ASportGameState::ServerTeamOneGoalScored_Validate(int Amount)
{
	if (Amount < 1 || Amount > 3)
		return false;

	return true;
}

void ASportGameState::ServerTeamOneGoalScored_Implementation(int Amount)
{
	TeamOneScore += Amount;
	OnTeamOneScored.Broadcast();
	OnTeamOneScoreChanged.Broadcast();
}

void ASportGameState::OnRep_SetTeamOneScore()
{
	OnTeamOneScored.Broadcast();
	OnTeamOneScoreChanged.Broadcast();
}


void ASportGameState::TeamTwoGoalScored(int Amount)
{
	ServerTeamTwoGoalScored(Amount);
}

bool ASportGameState::ServerTeamTwoGoalScored_Validate(int Amount)
{
	if (Amount < 1 || Amount > 3)
		return false;

	return true;
}

void ASportGameState::ServerTeamTwoGoalScored_Implementation(int Amount)
{
	TeamTwoScore += Amount;
	OnTeamTwoScored.Broadcast();
	OnTeamTwoScoreChanged.Broadcast();
}

void ASportGameState::OnRep_SetTeamTwoScore()
{
	OnTeamTwoScored.Broadcast();
	OnTeamTwoScoreChanged.Broadcast();
}
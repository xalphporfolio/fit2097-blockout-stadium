// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PhysicsPushButton.h"
#include "Components/BoxComponent.h"
#include "Field/FieldSystemComponent.h"
#include "GameFramework/Actor.h"
#include "MovingPlatform.generated.h"

UCLASS()
class LAB_BLOCKOUTSTADIUM_API AMovingPlatform : public AActor
{
	GENERATED_BODY()

	
public:
	AMovingPlatform();

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* PlatformBase;

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* Platform;

	UPROPERTY(EditDefaultsOnly)
	UPhysicsConstraintComponent* PhysicsConstraint;

	UPROPERTY(EditDefaultsOnly)
	UFieldSystemComponent* FieldSystemComponent;

	UPROPERTY(EditDefaultsOnly)
	UBoxComponent* FieldArea;

	UPROPERTY(EditDefaultsOnly)
	UBoxFalloff* BoxFalloff;

	UPROPERTY(EditDefaultsOnly)
	UUniformVector* UniformVector;

	UPROPERTY(EditDefaultsOnly)
	UCullingField* CullingField;

	UPROPERTY(EditDefaultsOnly)
	float PlatformBreakForce;
	UPROPERTY(EditDefaultsOnly)
    float PlatformBreakVelocity;

	UPROPERTY(EditInstanceOnly, meta=(MakeEditWidget))
	FVector PlatformTravelDistance;

	UPROPERTY(EditInstanceOnly)
	float PlatformMoveStrength;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* SBActivate;

	
protected:
	APhysicsPushButton* ButtonRef;

	bool IsActive;

	bool CanApplyForce;

	FTimerHandle PlatformForceTimer;


public:
	virtual void Tick(float DeltaSeconds) override;
	
	void ActivatePlatform();
	UFUNCTION(Server, Reliable)
	void ServerActivatePlatform();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastActivatePlatform();

	void SetPhysicsField() const;

	void PlaySound() const;
	
	
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnButtonPressed();

	UFUNCTION()
	void OnPlatformForceTimer();
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NiagaraSystem.h"
#include "Components/SceneComponent.h"
#include "SAKick.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnKick, FVector, HitVector);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnKickHit);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class LAB_BLOCKOUTSTADIUM_API USAKick : public USceneComponent
{
	GENERATED_BODY()

public:
	FOnKick OnKick;
	FOnKickHit OnKickHit;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float KickDistanceOffset;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float KickPower;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UNiagaraSystem* NSKick;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* SBKick;
	UPROPERTY(EditDefaultsOnly)
	USoundBase* SBHit;
	

public:
	USAKick();

	void Kick();
	UFUNCTION(Server, Reliable)	
	void ServerKick();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastKick(bool bHasHit = false);

	void PlayKickSound();
	UFUNCTION(Server, Reliable)
	void ServerPlayKickSound();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastPlayKickSound();

	void PlayHitSound();
	UFUNCTION(Server, Unreliable)
	void ServerPlayHitSound();
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastPlayHitSound();
};

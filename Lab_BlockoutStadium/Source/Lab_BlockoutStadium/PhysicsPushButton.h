// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "PhysicsPushButton.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnButtonPressed);


UCLASS()
class LAB_BLOCKOUTSTADIUM_API APhysicsPushButton : public AActor
{
	GENERATED_BODY()

	
public:	
	APhysicsPushButton();

	UPROPERTY(BlueprintAssignable)
	FOnButtonPressed OnButtonPressed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* ButtonBaseMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* ButtonMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPhysicsConstraintComponent* ButtonConstraint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ButtonSensitivity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bOneTimeUse;
	

public:
	virtual void Tick(float DeltaTime) override;
};

// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lab_BlockoutStadiumGameMode.generated.h"


UCLASS(minimalapi)
class ALab_BlockoutStadiumGameMode : public AGameModeBase
{
	GENERATED_BODY()


public:
	ALab_BlockoutStadiumGameMode();
};




// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingPlatform.h"

#include "Kismet/GameplayStatics.h"


AMovingPlatform::AMovingPlatform()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	NetPriority = 3.0f;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	PlatformBase = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlatformBase"));
	PlatformBase->SetupAttachment(RootComponent);

	Platform = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Platform"));
	Platform->SetupAttachment(RootComponent);

	Platform->SetSimulatePhysics(true);

	PhysicsConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("PhysicsConstraint"));
	PhysicsConstraint->SetupAttachment(RootComponent);

	FieldSystemComponent = CreateDefaultSubobject<UFieldSystemComponent>(TEXT("FieldSystemComponent"));
	FieldSystemComponent->SetupAttachment(Platform);

	FieldArea = CreateDefaultSubobject<UBoxComponent>(TEXT("FieldArea"));
	FieldArea->SetupAttachment(FieldSystemComponent);

	BoxFalloff = CreateDefaultSubobject<UBoxFalloff>(TEXT("BoxFalloff"));
	UniformVector = CreateDefaultSubobject<UUniformVector>(TEXT("UniformVector"));
	CullingField = CreateDefaultSubobject<UCullingField>(TEXT("CullingField"));

	PlatformBreakForce = 99999999.0f;
	PlatformBreakVelocity = 3000.0f;

	PlatformMoveStrength = 10.0f;

	IsActive = false;

	CanApplyForce = false;
}


void AMovingPlatform::BeginPlay()
{
	Super::BeginPlay();

	// get button
	TArray<UActorComponent*> ButtonSearchResult = GetComponentsByTag(UChildActorComponent::StaticClass(), FName("PushButton"));

	if (ButtonSearchResult.Num() > 0)
	{
		ButtonRef = Cast<APhysicsPushButton>(Cast<UChildActorComponent>(ButtonSearchResult[0])->GetChildActor());
		
		if (ButtonRef)
		    ButtonRef->OnButtonPressed.AddDynamic(this, &AMovingPlatform::OnButtonPressed);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("MovingPlatform could not find Button"));
	}

	// set up physics constraint
	PhysicsConstraint->SetLinearPositionDrive(false, false, false);
	PhysicsConstraint->SetLinearPositionTarget(PlatformTravelDistance);
	PhysicsConstraint->SetLinearDriveParams(PlatformMoveStrength, 1.0f, 0.0f);
}


void AMovingPlatform::OnButtonPressed()
{
	if (!HasAuthority())
		return;
		
	if (IsActive && CanApplyForce)
		return;
	
	ActivatePlatform();
}


void AMovingPlatform::OnPlatformForceTimer()
{
	CanApplyForce = false;
}


void AMovingPlatform::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (IsActive && CanApplyForce)
		SetPhysicsField();
}


void AMovingPlatform::ActivatePlatform()
{
	ServerActivatePlatform();
}

void AMovingPlatform::ServerActivatePlatform_Implementation()
{
	MulticastActivatePlatform();
}

void AMovingPlatform::MulticastActivatePlatform_Implementation()
{
	IsActive = true;
	CanApplyForce = true;

	GetWorld()->GetTimerManager().SetTimer(PlatformForceTimer, this, &AMovingPlatform::OnPlatformForceTimer, 5.0f, false);
	
	// bring up platform
	PhysicsConstraint->SetLinearPositionDrive(false, false, true);
	
	PlaySound();
}


void AMovingPlatform::SetPhysicsField() const
{
	// UE_LOG(LogTemp, Warning, TEXT("Moving Platform Applying Force"));
	BoxFalloff->SetBoxFalloff(PlatformBreakForce, 0, 1, 0, FieldArea->GetComponentTransform(), EFieldFalloffType::Field_FallOff_None);

	UniformVector->SetUniformVector(PlatformBreakVelocity, FVector::UpVector);
	CullingField->SetCullingField(BoxFalloff, UniformVector, EFieldCullingOperationType::Field_Culling_Outside);
	
	FieldSystemComponent->ApplyPhysicsField(true, EFieldPhysicsType::Field_ExternalClusterStrain, nullptr, BoxFalloff);
	FieldSystemComponent->ApplyPhysicsField(true, EFieldPhysicsType::Field_LinearForce, nullptr, CullingField);
}


void AMovingPlatform::PlaySound() const
{
	if (SBActivate)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), SBActivate, GetActorLocation());
	}
}

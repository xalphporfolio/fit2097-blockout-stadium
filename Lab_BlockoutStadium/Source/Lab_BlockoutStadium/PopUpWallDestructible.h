// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AnchorField.h"
#include "PhysicsPushButton.h"
#include "SetStaticDynamicField.h"
#include "GameFramework/Actor.h"
#include "GeometryCollection/GeometryCollectionComponent.h"
#include "GeometryCollection/GeometryCollectionObject.h"
#include "PopUpWallDestructible.generated.h"


UCLASS()
class LAB_BLOCKOUTSTADIUM_API APopUpWallDestructible : public AActor
{
	GENERATED_BODY()

	
public:	
	APopUpWallDestructible();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* WallBase;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* WallOutline;
	
	UActorComponent* DestructibleWall;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UGeometryCollection> GeoTest;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<class ASetStaticDynamicField> SetStaticDynamicFieldClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UMaterialInterface* MaterialClassOutline;
	UMaterialInstanceDynamic* MaterialOutlineInstance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UMaterialInterface* MaterialClassFilled;
	UMaterialInstanceDynamic* MaterialFilledInstance;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UMaterialInterface* MaterialClassFilledInside;
	UMaterialInstanceDynamic* MaterialFilledInsideInstance;

	bool IsFilled;
	
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	APhysicsPushButton* ConnectedButton;

	
protected:
	virtual void BeginPlay() override;

	void SpawnStateSettingField(int ChaosState);

	UFUNCTION()
	void OnButtonPressed();
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "SportGameState.h"
#include "ScoreWidget.generated.h"


UCLASS(Abstract)
class LAB_BLOCKOUTSTADIUM_API UScoreWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TeamOneScore;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TeamTwoScore;

	ASportGameState* GameState;


protected:
	UFUNCTION()
	void OnTeamOneScoreChanged();
	UFUNCTION()
	void OnTeamTwoScoreChanged();
	

public:
	virtual void NativeConstruct() override;
};

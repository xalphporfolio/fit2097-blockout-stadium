// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SAAcidPatch.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class LAB_BLOCKOUTSTADIUM_API USAAcidPatch : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<class ASlimeAcidPatch> AcidPatchClass;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float AcidTickDamage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float AcidDuration;


public:
	USAAcidPatch();

	void SpawnAcidPatch(const float Scale, const FVector SpawnLocation, const FRotator SpawnRotation,
		UPrimitiveComponent* ComponentToAttach, const FLinearColor PatchColor);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSpawnAcidPatch(const float Scale, const FVector SpawnLocation, const FRotator SpawnRotation,
		UPrimitiveComponent* ComponentToAttach, const FLinearColor PatchColor);
};

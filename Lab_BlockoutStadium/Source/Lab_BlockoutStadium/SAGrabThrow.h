// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Ball.h"
#include "NiagaraSystem.h"
#include "SASizeModifier.h"
#include "Components/BoxComponent.h"
#include "SAGrabThrow.generated.h"

class ALab_BlockoutStadiumCharacter;


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHovered);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGrabbed);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnThrown);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnKnocked);


UCLASS()
class LAB_BLOCKOUTSTADIUM_API USAGrabThrow : public UBoxComponent
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	ALab_BlockoutStadiumCharacter* PlayerRef;
	UPROPERTY()
	USASizeModifier* SizeModifierComponent;

	
public:
	FOnHovered OnHovered;
	FOnGrabbed OnGrabbed;
	FOnThrown OnThrown;
	FOnKnocked OnKnocked;
	
	UPROPERTY(ReplicatedUsing = OnRep_HoveredBall)
	ABall* HoveredBall;
	UPROPERTY(ReplicatedUsing = OnRep_GrabbedBall)
	ABall* GrabbedBall;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<float> BallHoldOffsets;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float ThrowForce;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UNiagaraSystem* NSThrow;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* SBThrow;
	

protected:
	virtual void BeginPlay() override;

	virtual void OnComponentDestroyed(bool bDestroyingHierarchy) override;

	UFUNCTION()
	void OnGrabHitBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnGrabHitBoxOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void OnBallKnockedOff();
	UFUNCTION()
	void OnBallGrabbed();
	

public:
	USAGrabThrow();

	void InitComponent(ALab_BlockoutStadiumCharacter* Player, USASizeModifier* SizeModifier);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	bool IsGrabbing() const;

	UFUNCTION()
	void OnRep_HoveredBall();
	UFUNCTION()
	void OnRep_GrabbedBall();
	
	void GrabAndThrow();
	UFUNCTION(Server, Reliable)
	void ServerGrabAndThrow();

	void GrabBall();
	UFUNCTION(Server, Reliable)
	void ServerGrabBall();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastGrabBall();
	
	void ThrowBall();
	UFUNCTION(Server, Reliable)
	void ServerThrowBall();
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastThrowBall(const FVector ParticleSpawnPos, const FRotator ParticleSpawnRot);

	void ReleaseBall();
	
	void SetGrabBallHighlight(bool bIsHighlighted);
	UFUNCTION(Server, Reliable)
	void ServerSetGrabBallHighlight(bool bIsHighlighted);
	UFUNCTION(Client, Reliable)
	void ClientSetGrabBallHighlight(bool bIsHighlighted);

	void PlayThrowSound();
	UFUNCTION(Server, Unreliable)
	void ServerPlayThrowSound();
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastPlayThrowSound();
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "StickyTiles.h"


// Sets default values
AStickyTiles::AStickyTiles()
{
	PrimaryActorTick.bCanEverTick = true;

	TileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TileMesh"));
	TileMesh->SetupAttachment(RootComponent);
	TileMesh->SetMobility(EComponentMobility::Static);

	HitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("HitBox"));
	HitBox->SetupAttachment(TileMesh);

	VelocityDampFactor = 0.05f;
}


void AStickyTiles::BeginPlay()
{
	Super::BeginPlay();

	HitBox->OnComponentBeginOverlap.AddDynamic(this, &AStickyTiles::OnHitBoxOverlapBegin);
}


void AStickyTiles::OnHitBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != this)
	{
		if (const ABall* OverlapBall = Cast<ABall>(OtherActor))
		{
			FVector CurrentLinearVelocity = OverlapBall->BallMesh->GetPhysicsLinearVelocity();
			CurrentLinearVelocity *= VelocityDampFactor;
			CurrentLinearVelocity.Z = 0.0f;

			FVector CurrentAngularVelocity = OverlapBall->BallMesh->GetPhysicsAngularVelocityInRadians();
			CurrentAngularVelocity *= VelocityDampFactor;
			
			OverlapBall->BallMesh->SetPhysicsLinearVelocity(CurrentLinearVelocity);
			OverlapBall->BallMesh->SetPhysicsAngularVelocityInDegrees(CurrentAngularVelocity);
		}
	}
}


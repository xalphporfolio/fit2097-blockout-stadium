// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Lab_BlockoutStadium : ModuleRules
{
	public Lab_BlockoutStadium(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "Niagara", "CableComponent" });
	}
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "SAAcidPatch.h"

#include "SlimeAcidPatch.h"


USAAcidPatch::USAAcidPatch()
{
	PrimaryComponentTick.bCanEverTick = false;

	AcidTickDamage = 5000.0f;
	AcidDuration = 10.0f;
}


void USAAcidPatch::SpawnAcidPatch(const float Scale, const FVector SpawnLocation, const FRotator SpawnRotation,
	UPrimitiveComponent* ComponentToAttach, const FLinearColor PatchColor)
{
	ServerSpawnAcidPatch(Scale, SpawnLocation, SpawnRotation, ComponentToAttach, PatchColor);
}

bool USAAcidPatch::ServerSpawnAcidPatch_Validate(const float Scale, FVector SpawnLocation, FRotator SpawnRotation,
	UPrimitiveComponent* ComponentToAttach, FLinearColor PatchColor)
{
	if (Scale > 0.0f && Scale < 5.0f)
	{
		return true;
	}
	
	return false;
}

void USAAcidPatch::ServerSpawnAcidPatch_Implementation(const float Scale, const FVector SpawnLocation, const FRotator SpawnRotation,
	UPrimitiveComponent* ComponentToAttach, const FLinearColor PatchColor)
{
	if (!AcidPatchClass)
		return;
	
	FActorSpawnParameters ActorSpawnParameters;
	ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			
	ASlimeAcidPatch* SpawnedPatch = GetWorld()->SpawnActor<ASlimeAcidPatch>(AcidPatchClass, SpawnLocation, SpawnRotation, ActorSpawnParameters);

	// attach to the component slime lands on
	if (ComponentToAttach)
	{
		SpawnedPatch->AttachToComponent(ComponentToAttach, FAttachmentTransformRules::KeepWorldTransform);
	}
			
	SpawnedPatch->SetAcidPatch(Scale, PatchColor, FMath::FRandRange(0.0f, 359.0f), AcidDuration, AcidTickDamage);
}

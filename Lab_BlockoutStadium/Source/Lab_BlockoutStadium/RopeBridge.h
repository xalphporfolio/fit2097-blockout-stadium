// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "RopeBridge.generated.h"


UCLASS()
class LAB_BLOCKOUTSTADIUM_API ARopeBridge : public AActor
{
	GENERATED_BODY()
	
public:	
	ARopeBridge();

	// constraints
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPhysicsConstraintComponent* FrontEndConstraintRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPhysicsConstraintComponent* FrontEndConstraintLeft;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UPhysicsConstraintComponent* RightConstraint1;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UPhysicsConstraintComponent* RightConstraint2;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UPhysicsConstraintComponent* RightConstraint3;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UPhysicsConstraintComponent* RightConstraint4;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UPhysicsConstraintComponent* LeftConstraint1;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UPhysicsConstraintComponent* LeftConstraint2;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UPhysicsConstraintComponent* LeftConstraint3;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UPhysicsConstraintComponent* LeftConstraint4;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPhysicsConstraintComponent* BackEndConstraintRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPhysicsConstraintComponent* BackEndConstraintLeft;

	// planks
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* Plank1;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* Plank2;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* Plank3;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* Plank4;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* Plank5;

};

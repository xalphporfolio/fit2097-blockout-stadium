// Fill out your copyright notice in the Description page of Project Settings.


#include "SetStaticDynamicField.h"


ASetStaticDynamicField::ASetStaticDynamicField()
{
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collision"));
	BoxCollision->SetupAttachment(FieldSystemComponent);

	UniformInteger = CreateDefaultSubobject<UUniformInteger>(TEXT("Uniform Integer"));
	CullingField = CreateDefaultSubobject<UCullingField>(TEXT("Culling Field"));
	BoxFalloff = CreateDefaultSubobject<UBoxFalloff>(TEXT("Ball Falloff"));
}


void ASetStaticDynamicField::CreateField(int ChaosState, FVector FieldSize)
{
	BoxCollision->SetRelativeScale3D(FieldSize);
	
	BoxFalloff->SetBoxFalloff(1.0f, 0.0f, 1.0f, 0.0f, BoxCollision->GetComponentTransform(), EFieldFalloffType::Field_FallOff_None);

	UniformInteger->SetUniformInteger(ChaosState);

	CullingField->SetCullingField(BoxFalloff, UniformInteger, EFieldCullingOperationType::Field_Culling_Outside);

	FieldSystemComponent->ApplyPhysicsField(true, EFieldPhysicsType::Field_DynamicState, nullptr, CullingField);

	Destroy();
}

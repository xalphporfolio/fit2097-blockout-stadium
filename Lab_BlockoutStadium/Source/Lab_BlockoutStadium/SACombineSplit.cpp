// Fill out your copyright notice in the Description page of Project Settings.


#include "SACombineSplit.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Net/UnrealNetwork.h"
#include "Lab_BlockoutStadiumCharacter.h"
#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"


USACombineSplit::USACombineSplit()
{
	PrimaryComponentTick.bCanEverTick = false;
	
	bCanRecombine = true;
	bIsRecombining = false;
}


void USACombineSplit::InitComponent(ALab_BlockoutStadiumCharacter* Player, USASizeModifier* SizeModifier)
{
	PlayerRef = Player;
	SizeModifierCompRef = SizeModifier;

	if (!PlayerRef)
		UE_LOG(LogTemp, Warning, TEXT("CombineSplit Component - No PlayerRef received"));

	if (!SizeModifierCompRef)
		UE_LOG(LogTemp, Warning, TEXT("CombineSplit Component - No SizeModifierCompRef received"));
}


void USACombineSplit::BeginPlay()
{
	Super::BeginPlay();

	OnComponentBeginOverlap.AddDynamic(this, &USACombineSplit::OnCombineHitBoxOverlapBegin);
}


void USACombineSplit::OnCanRecombineTimer()
{
	if (!PlayerRef)
		return;
	
	if (!PlayerRef->HasAuthority())
		return;
	
	bCanRecombine = true;
	GetWorld()->GetTimerManager().ClearTimer(CanRecombineTimerHandle);
}


void USACombineSplit::OnCombineHitBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                                  UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!PlayerRef || !SizeModifierCompRef)
	{
		UE_LOG(LogTemp, Warning, TEXT("USACombineSplit component does not have player ref and or size modifier component"));
		return;
	}
	
	if (OtherActor && OtherActor != PlayerRef)
	{
		if (!PlayerRef->HasAuthority())
			return;
		
		ALab_BlockoutStadiumCharacter* CollidedChar = Cast<ALab_BlockoutStadiumCharacter>(OtherActor);

		if (bCanRecombine && CollidedChar && CollidedChar->GetCurrentTeam() == PlayerRef->GetCurrentTeam() &&
			CollidedChar->SizeModifierComponent->GetCurrentSize() == SizeModifierCompRef->GetCurrentSize() && CollidedChar->CombineSplitComponent->bCanRecombine)
		{
			if (CollidedChar->SizeModifierComponent->GetCurrentSize() == EActorSize::SIZE_LARGE)
				return;

			// cannot recombine if in grabbing state
			if (PlayerRef->CurrentState == EPlayerState::STATE_GRABBING || CollidedChar->CurrentState == EPlayerState::STATE_GRABBING)
				return;

			// player controlled and collided is not player controlled
			if (PlayerRef->IsPlayerControlled() && !CollidedChar->IsPlayerControlled())
			{
				CombineWithCollidedChar(CollidedChar);
				return;
			}

			// both overlapping bodies are not player controlled
			if (!PlayerRef->IsPlayerControlled() && !CollidedChar->IsPlayerControlled())
			{
				if (CollidedChar->GetVelocity().Length() < PlayerRef->GetVelocity().Length())
				{
					CombineWithCollidedChar(CollidedChar);
					return;
				}
			}
		}
	}
}


void USACombineSplit::CombineWithCollidedChar(ALab_BlockoutStadiumCharacter* CollidedChar)
{
	if (!PlayerRef || !SizeModifierCompRef)
		return;
	
	CollidedChar->CombineSplitComponent->bIsRecombining = true;
	CollidedChar->Destroy();
	SizeModifierCompRef->IncreaseSize();

	// UE_LOG(LogTemp, Warning, TEXT("Player recombined"));

	PlaySound(PlayerRef->GetVelocity().Length());
	
	OnCombined.Broadcast();
}


void USACombineSplit::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USACombineSplit, bCanRecombine);
}


void USACombineSplit::Split(const FVector SplitDirection, const float SplitPushForce)
{
	if (!PlayerRef || !SizeModifierCompRef)
	{
		UE_LOG(LogTemp, Warning, TEXT("USACombineSplit component does not have player ref and or size modifier component"));
		return;
	}
	
	ServerSplit(SplitDirection, SplitPushForce);
}

bool USACombineSplit::ServerSplit_Validate(const FVector SplitDirection, const float SplitPushForce)
{
	if (SplitPushForce > 5000.0f)
		return false;
	
	return true;
}

void USACombineSplit::ServerSplit_Implementation(const FVector SplitDirection, const float SplitPushForce)
{
	if (!PlayerRef || !SizeModifierCompRef)
		return;
	
	if (!PlayerCharacter)
		return;

	if (SizeModifierCompRef->GetCurrentSize() == EActorSize::SIZE_SMALL)
		return;

	bCanRecombine = false;
	GetWorld()->GetTimerManager().SetTimer(CanRecombineTimerHandle, this, &USACombineSplit::OnCanRecombineTimer, 0.3f, false);
	
	const FRotator SpawnRotation = PlayerRef->GetActorRotation();
	const FVector SpawnLocation = PlayerRef->GetActorLocation();
	
	FActorSpawnParameters ActorSpawnParameters;
	ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	
	if (ALab_BlockoutStadiumCharacter* SpawnedCharacter = GetWorld()->SpawnActor<ALab_BlockoutStadiumCharacter>(PlayerCharacter,
		SpawnLocation, SpawnRotation, ActorSpawnParameters))
	{
		SpawnedCharacter->SetLastCharacterController(PlayerRef->LastCharacterController);
		
		SizeModifierCompRef->ReduceSize();
		SpawnedCharacter->SizeModifierComponent->SetSize(SizeModifierCompRef->GetCurrentSize());
		
		const FVector PushForce = SplitDirection * SplitPushForce;
		SpawnedCharacter->GetCharacterMovement()->AddImpulse(PushForce, true);

		SpawnedCharacter->SwitchState(EPlayerState::STATE_FLYING);

		// UE_LOG(LogTemp, Warning, TEXT("Spawned New Player Character"));

		PlaySound(PlayerRef->GetVelocity().Length());
		
		OnSplit.Broadcast();
	}
}


void USACombineSplit::PlaySound(const float ColVel)
{
	if (!PlayerRef)
		return;
	
	if (PlayerRef->HasNetOwner())
		ServerPlaySound(ColVel);
}

void USACombineSplit::ServerPlaySound_Implementation(const float ColVel)
{
	MulticastPlaySound(ColVel);
}

void USACombineSplit::MulticastPlaySound_Implementation(const float ColVel)
{
	if (SBSplitCombine)
	{
		float RemapVel =  FMath::Lerp(0.2f, 1.0f,
			UKismetMathLibrary::NormalizeToRange(ColVel, 0.0f, 300.0f));

		RemapVel = FMath::Clamp(RemapVel, 0.0f, 1.0f);
		
		UAudioComponent* SBAudio = UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SBSplitCombine, GetComponentLocation());
		SBAudio->SetFloatParameter(FName("CollisionVelNorm"), RemapVel);
		SBAudio->SetTriggerParameter(FName("On Collide"));
	}
}
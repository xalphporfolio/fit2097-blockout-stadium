// Copyright Epic Games, Inc. All Rights Reserved.

#include "Lab_BlockoutStadiumCharacter.h"

#include "Lab_BlockoutStadium.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "PlayerCharacterController.h"
#include "Ball.h"
#include "SlimeAcidPatch.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"


ALab_BlockoutStadiumCharacter::ALab_BlockoutStadiumCharacter()
{
	bReplicates = true;
	bAlwaysRelevant = true;
	
	GetCapsuleComponent()->InitCapsuleSize(60.f, 60.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; 
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); 
	
	GetCharacterMovement()->AirControl = 0.8f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f; 
	CameraBoom->bUsePawnControlRotation = true; 

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); 
	FollowCamera->bUsePawnControlRotation = false; 
	
	// set our turn rate for input
	TurnRateGamepad = 50.f;

	// size ability component 
	SizeModifierComponent = CreateDefaultSubobject<USASizeModifier>(TEXT("SizeModifierComponent"));
	SizeModifierComponent->SetIsReplicated(true);

	// combine split ability component
	CombineSplitComponent = CreateDefaultSubobject<USACombineSplit>(TEXT("CombineSplitComponent"));
	CombineSplitComponent->SetIsReplicated(true);
	CombineSplitComponent->InitCapsuleSize(40.0f, 40.0f);
	CombineSplitComponent->SetupAttachment(RootComponent);

	// jump ability component
	JumpLaunchComponent = CreateDefaultSubobject<USAJumpLaunch>(TEXT("JumpLaunchComponent"));
	JumpLaunchComponent->SetIsReplicated(true);

	// kick ability component 
	KickComponent = CreateDefaultSubobject<USAKick>(TEXT("KickComponent"));
	KickComponent->SetupAttachment(GetMesh());

	// grab and throw ability component 
	GrabThrowComponent = CreateDefaultSubobject<USAGrabThrow>(TEXT("GrabThrowComponent"));
	GrabThrowComponent->SetIsReplicated(true);
	GrabThrowComponent->SetupAttachment(GetMesh());

	// acid patch ability component 
	AcidPatchComponent = CreateDefaultSubobject<USAAcidPatch>(TEXT("AcidPatchComponent"));
	
	LastCharacterController = nullptr;

	// jiggle shader
	PreviousVelocity = FVector::Zero();
	CurrentMaterialVelocity = FVector::Zero();
	
	bVerticalBouncing = false;
	MaxVerticalBounce = 500.0f;
	
	bHorizontalBouncing = false;
	MaxHorizontalBounce = 150.0f;
	LastHorizontalDir = FVector::Zero();
	
	// states
	CurrentState = EPlayerState::STATE_NORMAL;
	PreviousState = CurrentState;

	// speed multipliers
	SpeedMultiplier = 1.0f;
	AimSpeedMultiplier = 0.5f;
	FlyingSpeedMultiplier = 0.2f;

	// size
	MoveSpeeds = {600.0f, 750.0f, 850.0f};

	// collision particles
	SlimePiecesVelocityThreshold = 100.0f;
	SlimePiecesAmount = {3, 3, 3};

	// bouncing
	BounceForceMultiplier = {2.0f, 1.5f, 1.0f};

	MinPlayerBounce = 200.0f;
	MaxPlayerBounce = 1200.0f;
	
	MinBallUpBounce = 600.0f;
	MaxBallUpBounce = 900.0f;
	
	MinBallForwardBounce = 400.0f;
	MaxBallForwardBounce = 1000.0f;
	
	GetCharacterMovement()->MaxWalkSpeed = MoveSpeeds[static_cast<int>(SizeModifierComponent->GetCurrentSize())];

	// shooting
	DefaultCameraDistance = 500.0f;
	DefaultCameraOffset = FVector(0, 0, 10);

	AimingCameraDistance = 300.0f;
	AimingCameraOffset = FVector(0, 100.0f, 40);

	ShootSplitForce = 1250.0f;

	// grabbing
	GrabSpeedMultiplier = 0.25f;

	// launch
	ChargingCameraDistance = 700.0f;;
	ChargingCameraOffset = FVector(0, 0, 0);

	// acid patches
	AcidParticleVelocityThreshold = 410.0f;
	AcidPatchVelocityThreshold = 650.0f;
	
	AcidParticleMinSize = 0.1f;
	AcidParticleMaxSize = 0.3f;

	// audio
	bIsMoveSoundsPlaying = false;
}


void ALab_BlockoutStadiumCharacter::BeginPlay()
{
	Super::BeginPlay();

	// UE_LOG(LogTemp, Warning, TEXT("Player Controller: %s"), *GetController()->GetActorLabel());

	GameState = Cast<ASportGameState>(GetWorld()->GetGameState());
	
	// create dynamic materials
	if (MaterialClass)
	{
		MaterialInstance = UMaterialInstanceDynamic::Create(MaterialClass, this);
		GetMesh()->SetMaterial(0, MaterialInstance);
	}

	// connect delegates
	GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &ALab_BlockoutStadiumCharacter::OnCapsuleHit);

	SizeModifierComponent->OnSizeChanged.AddDynamic(this, &ALab_BlockoutStadiumCharacter::OnSizeChanged);

	CombineSplitComponent->InitComponent(this, SizeModifierComponent);
	CombineSplitComponent->OnCombined.AddDynamic(this, &ALab_BlockoutStadiumCharacter::OnCombined);
	CombineSplitComponent->OnSplit.AddDynamic(this, &ALab_BlockoutStadiumCharacter::OnSplit);

	JumpLaunchComponent->InitComponent(this, SizeModifierComponent);
	JumpLaunchComponent->OnCharacterJump.AddDynamic(this, &ALab_BlockoutStadiumCharacter::OnCharacterJump);
	JumpLaunchComponent->OnCharacterLaunched.AddDynamic(this, &ALab_BlockoutStadiumCharacter::OnCharacterLaunched);
	
	KickComponent->OnKickHit.AddDynamic(this, &ALab_BlockoutStadiumCharacter::OnKickHit);

	GrabThrowComponent->InitComponent(this, SizeModifierComponent);
	GrabThrowComponent->OnGrabbed.AddDynamic(this, &ALab_BlockoutStadiumCharacter::OnGrabbed);
	GrabThrowComponent->OnThrown.AddDynamic(this, &ALab_BlockoutStadiumCharacter::OnThrown);
	
	if (GameState)
	{
		GameState->OnRoundReset.AddDynamic(this, &ALab_BlockoutStadiumCharacter::OnRoundReset);
	}

	this->OnDestroyed.AddDynamic(this, &ALab_BlockoutStadiumCharacter::OnDestroy);
}


void ALab_BlockoutStadiumCharacter::UnPossessed()
{
	Super::UnPossessed();

	AimEnd();
	JumpLaunchComponent->CancelCharge();
	
	// UE_LOG(LogTemp, Warning, TEXT("Previous slime current state: %s"), *UEnum::GetValueAsString(CurrentState));
}


void ALab_BlockoutStadiumCharacter::OnRoundReset()
{
	if (!HasAuthority())
		return;
	
	this->OnDestroyed.RemoveDynamic(this, &ALab_BlockoutStadiumCharacter::OnDestroy);
	this->Destroy();
}


void ALab_BlockoutStadiumCharacter::OnDestroy(AActor* DestroyedActor)
{
	// Note: OnDestroy gets called after controllers are unpossessed

	if (!HasAuthority())
		return;

	if (CombineSplitComponent->bIsRecombining)
		return;
	
	if (DestroyedActor == this)
	{
		// UE_LOG(LogTemp, Warning, TEXT("Slime is being destroyed"));
		
		this->OnDestroyed.RemoveDynamic(this, &ALab_BlockoutStadiumCharacter::OnDestroy);

		if (!LastCharacterController->GetPawn())
		{
			GameState->SetUpPlayer(LastCharacterController->PlayerState);
			LastCharacterController->PossessedPlayer->SizeModifierComponent->SetSize(SizeModifierComponent->GetCurrentSize());

			//UE_LOG(LogTemp, Warning, TEXT("Slime is being destroyed - Player Controlled | Size: %s"), *UEnum::GetValueAsString(CurrentSize));
		}
		else
		{
			ALab_BlockoutStadiumCharacter* SpawnedPlayer = GameState->SpawnNewPlayer(GetCurrentTeam());
			
			SpawnedPlayer->SizeModifierComponent->SetSize(SizeModifierComponent->GetCurrentSize());
			SpawnedPlayer->SetLastCharacterController(LastCharacterController);

			//UE_LOG(LogTemp, Warning, TEXT("Slime is being destroyed - Not Player Controlled"));
		}
	}
}


void ALab_BlockoutStadiumCharacter::OnCapsuleHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
                                                 UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!HasAuthority())
		return;

	// switch to normal state when on ground after flying
	if (GetCharacterMovement()->IsMovingOnGround() && CurrentState == EPlayerState::STATE_FLYING)
	{
		if (GrabThrowComponent->IsGrabbing())
		{
			SwitchState(EPlayerState::STATE_GRABBING);
		}
		else
		{
			SwitchState(EPlayerState::STATE_NORMAL);	
		}
		
		// spawn acid patch and attach it onto hit object, but not other players or the ball
		if ((OtherActor && OtherActor != this) &&
			(!OtherActor->IsA(ALab_BlockoutStadiumCharacter::StaticClass()) && !OtherActor->IsA(ABall::StaticClass())) &&
			(PreviousVelocity.Length() > AcidPatchVelocityThreshold))
		{
			const FVector SpawnLocation = Hit.Location;
			const FRotator SpawnRotation = this->GetActorRotation();
			
			// attach to the component slime lands on
			if (HitComponent->GetAttachmentRootActor() == this)
			{
				AcidPatchComponent->SpawnAcidPatch(SizeModifierComponent->GetCurrentScale().X, SpawnLocation,
					SpawnRotation, OtherComp, GetCurrentColour());
			}
			else
			{
				AcidPatchComponent->SpawnAcidPatch(SizeModifierComponent->GetCurrentScale().X, SpawnLocation,
					SpawnRotation, HitComponent, GetCurrentColour());
				
			}
		}
		
		// UE_LOG(LogTemp, Warning, TEXT("Landed on ground and switching to normal state"));
		// UE_LOG(LogTemp, Warning, TEXT("Landed on ground | Velocity: %f"), PreviousVelocity.Length());
	}

	// spawn particles after colliding
	const FVector CurrentVelocity = GetVelocity();
	
	if ((PreviousVelocity.Length() > SlimePiecesVelocityThreshold) &&
		((FMath::IsNearlyEqual(CurrentVelocity.X, 0) && !FMath::IsNearlyEqual(PreviousVelocity.X, 0)) ||
		 (FMath::IsNearlyEqual(CurrentVelocity.Y, 0) && !FMath::IsNearlyEqual(PreviousVelocity.Y, 0))||
		 (FMath::IsNearlyEqual(CurrentVelocity.Z, 0) && !FMath::IsNearlyEqual(PreviousVelocity.Z, 0))))
	{
		FVector ColDir = PreviousVelocity * -1;
		ColDir.Normalize();

		SpawnCollisionParticles(ColDir);
		PlayCollisionSound(PreviousVelocity.Length());
	}
	
	if (!OtherActor || OtherActor == this)
		return;
	
	// bouncing off other players
	if (Cast<ALab_BlockoutStadiumCharacter>(OtherActor))
		HandlePlayerOnPlayerBounce(Cast<ALab_BlockoutStadiumCharacter>(OtherActor), Hit);

	// knocking up ball
	if (Cast<ABall>(OtherActor) && CurrentState == EPlayerState::STATE_FLYING)
		HandlePlayerBallHit(Cast<ABall>(OtherActor), Hit);
}


void ALab_BlockoutStadiumCharacter::HandlePlayerOnPlayerBounce(const ALab_BlockoutStadiumCharacter* HitPlayer, const FHitResult& Hit)
{
	FVector InDir = HitPlayer->GetActorLocation() - GetActorLocation();
	InDir.Normalize(); 
	
	float ApplyingForce = ((GetVelocity().Length() + HitPlayer->GetVelocity().Length()) * 0.5f) * BounceForceMultiplier[static_cast<int>(SizeModifierComponent->GetCurrentSize())];
	ApplyingForce = FMath::Clamp(ApplyingForce, MinPlayerBounce, MaxPlayerBounce);
	
	const FVector ApplyingVector = InDir * ApplyingForce;

	HitPlayer->GetCharacterMovement()->AddImpulse(ApplyingVector, true);

	PlayCollisionSound(ApplyingForce);
	
	// UE_LOG(LogTemp, Warning, TEXT("Player (%s) bounced against (%s) | Vector: %s | Force: %f"), *GetActorLabel(), *HitPlayer->GetActorLabel(), *ApplyingVector.ToString(), ApplyingVector.Length());
}


void ALab_BlockoutStadiumCharacter::HandlePlayerBallHit(ABall* HitBall, const FHitResult& Hit)
{
	// bounce ball
	const float HitForce = (GetVelocity().Length() * BounceForceMultiplier[static_cast<int>(SizeModifierComponent->GetCurrentSize())]);
	FVector Impulse = FVector::Zero();
	
	if (HitBall->IsOnGround())
	{
		// UE_LOG(LogTemp, Warning, TEXT("Player will knock ball up"));
		
		Impulse = FVector::UpVector * FMath::Clamp(HitForce * 0.8f, MinBallUpBounce, MaxBallUpBounce);
	
		// reset ball velocity to ensure it only bounces up
		HitBall->BallMesh->SetPhysicsLinearVelocity(FVector::Zero());
	
		if (LastCharacterController && LastCharacterController->PlayerState)
		{
			if (Cast<ASportPlayerState>(LastCharacterController->PlayerState))
			{
				Cast<ASportPlayerState>(LastCharacterController->PlayerState)->AddTimesKnockedBallUp(1);
			}
			else
			{
				// UE_LOG(LogTemp, Warning, TEXT("Could not cast player state"));
			}
		}
		else
		{
			// UE_LOG(LogTemp, Warning, TEXT("No Player State"));
		}
	}
	else
	{
		// UE_LOG(LogTemp, Warning, TEXT("Player will knock ball forward"));
		
		FVector InDir = HitBall->GetActorLocation() - GetActorLocation();
		InDir.Normalize();
		
		Impulse = InDir * FMath::Clamp(HitForce * 0.2f, MinBallForwardBounce, MaxBallForwardBounce);
	
		if (LastCharacterController && LastCharacterController->PlayerState)
		{
			if (Cast<ASportPlayerState>(LastCharacterController->PlayerState))
			{
				Cast<ASportPlayerState>(LastCharacterController->PlayerState)->AddTimesKnockedBallForward(1);
			}
			else
			{
				//UE_LOG(LogTemp, Warning, TEXT("Could not cast player state"));
			}
		}
		else
		{
			//UE_LOG(LogTemp, Warning, TEXT("No Player State"));
		}
	}
	
	HitBall->BallMesh->AddImpulse(Impulse, "", true);

	//UE_LOG(LogTemp, Warning, TEXT("Player hit ball: Force %f"), Impulse.Length());

	// bounce player back
	const float BounceForce = FMath::Clamp(HitForce * 0.2f, MinPlayerBounce, MaxPlayerBounce);
	
	FVector BounceDir = GetActorLocation() - HitBall->GetActorLocation();
	BounceDir.Normalize();
	
	const FVector BounceImpulse = BounceForce * BounceDir;
	
	GetCharacterMovement()->AddImpulse(BounceImpulse, true);
	SwitchState(EPlayerState::STATE_FLYING);

	PlayCollisionSound(BounceForce);
	
	//UE_LOG(LogTemp, Warning, TEXT("Player hit back by ball: Force %f"), BounceImpulse.Length());
}


void ALab_BlockoutStadiumCharacter::OnSizeChanged(const EActorSize NewSize)
{
	// movement
	// GetCharacterMovement()->JumpZVelocity = JumpSpeeds[static_cast<int>(NewSize)];
	GetCharacterMovement()->MaxWalkSpeed = MoveSpeeds[static_cast<int>(NewSize)] * SpeedMultiplier;

	// audio
	if (MoveAudioComponent)
		MoveAudioComponent->SetIntParameter(FName("CurrentSize"), static_cast<int>(NewSize));
}


void ALab_BlockoutStadiumCharacter::OnCombined()
{
	SpawnCollisionParticles(FVector::UpVector);
}


void ALab_BlockoutStadiumCharacter::OnSplit()
{
	SpawnCollisionParticles(FVector::UpVector);
}


void ALab_BlockoutStadiumCharacter::OnCharacterJump()
{
	// UE_LOG(LogTemp, Warning, TEXT("Jump Component: Character Jumped"));
	
	StopMoveSounds();
}

void ALab_BlockoutStadiumCharacter::OnCharacterLaunched()
{
	// UE_LOG(LogTemp, Warning, TEXT("Jump Component: Character Launched"));
	
	ResetCamera();

	if (IsLocallyControlled())
	{
		if (GetPlayerState())
		{
			if (Cast<ASportPlayerState>(GetPlayerState()))
			{
				Cast<ASportPlayerState>(GetPlayerState())->AddTimesLaunched(1);
			}
		}
	}
}


void ALab_BlockoutStadiumCharacter::OnKickHit()
{
	if (IsLocallyControlled())
	{
		if (GetPlayerState())
		{
			if (Cast<ASportPlayerState>(GetPlayerState()))
			{
				Cast<ASportPlayerState>(GetPlayerState())->AddKicks(1);
			}
		}
	}	
}


void ALab_BlockoutStadiumCharacter::OnGrabbed()
{
	if (IsLocallyControlled())
	{
		if (GetPlayerState())
		{
			if (Cast<ASportPlayerState>(GetPlayerState()))
			{
				Cast<ASportPlayerState>(GetPlayerState())->AddBallGrabs(1);
			}
		}
	}
}

void ALab_BlockoutStadiumCharacter::OnThrown()
{
	if (IsLocallyControlled())
	{
		// UE_LOG(LogTemp, Warning, TEXT("Player has thrown ball"));
		
		if (GetPlayerState())
		{
			if (Cast<ASportPlayerState>(GetPlayerState()))
			{
				Cast<ASportPlayerState>(GetPlayerState())->AddBallThrows(1);
			}
		}
	}
}


void ALab_BlockoutStadiumCharacter::UpdateBodyJiggle(float DeltaTime)
{
	// controlling jiggle for slime material
	if (MaterialInstance)
	{
		const FVector CurrentVelocity = GetVelocity();

		// push down slime body when charging
		if (CurrentState == EPlayerState::STATE_CHARGING)
		{
			CurrentMaterialVelocity = UKismetMathLibrary::VInterpTo(CurrentMaterialVelocity, FVector(0, 0, MaxVerticalBounce), DeltaTime, 10);
		}
		else
		{
			// setting vertical bounce
			if (bVerticalBouncing)
			{
				CurrentMaterialVelocity.Z = 150 * GetBounceSinDamp(GetWorld()->GetTimerManager().GetTimerElapsed(VerticalBounceTimerHandle));

				if (!FMath::IsNearlyEqual(CurrentVelocity.Z, 0) && FMath::IsNearlyEqual(PreviousVelocity.Z, 0))
				{
					GetWorld()->GetTimerManager().ClearTimer(VerticalBounceTimerHandle);
					bVerticalBouncing = false;
				}
			}
			else
			{
				if (!bVerticalBouncing && FMath::IsNearlyEqual(CurrentVelocity.Z, 0) && !FMath::IsNearlyEqual(PreviousVelocity.Z, 0))
				{
					bVerticalBouncing = true;
					GetWorld()->GetTimerManager().SetTimer(VerticalBounceTimerHandle, this, &ALab_BlockoutStadiumCharacter::OnVerticalBounceTimer, 2.0f, false);
				}
				else
				{
					CurrentMaterialVelocity.Z = CurrentVelocity.Z;
				}
			}

			// setting horizontal bounce
			if (bHorizontalBouncing)
			{
				const float BounceAmount = GetBounceSinDamp(GetWorld()->GetTimerManager().GetTimerElapsed(HorizontalBounceTimerHandle));
				CurrentMaterialVelocity.X = MaxHorizontalBounce * BounceAmount * LastHorizontalDir.X;
				CurrentMaterialVelocity.Y = MaxHorizontalBounce * BounceAmount * LastHorizontalDir.Y;

				if ((!FMath::IsNearlyEqual(CurrentVelocity.X, 0) || !FMath::IsNearlyEqual(CurrentVelocity.Y, 0)) &&
					(FMath::IsNearlyEqual(PreviousVelocity.X, 0) && FMath::IsNearlyEqual(PreviousVelocity.Y, 0)))
				{
					GetWorld()->GetTimerManager().ClearTimer(HorizontalBounceTimerHandle);
					bHorizontalBouncing = false;
				}
			}
			else
			{
				if (!bHorizontalBouncing &&
					(FMath::IsNearlyEqual(CurrentVelocity.X, 0) && FMath::IsNearlyEqual(CurrentVelocity.Y, 0)) &&
					(!FMath::IsNearlyEqual(PreviousVelocity.X, 0) || !FMath::IsNearlyEqual(PreviousVelocity.Y, 0)))
				{
					bHorizontalBouncing = true;
					LastHorizontalDir = PreviousVelocity;
					LastHorizontalDir.Normalize();
				
					GetWorld()->GetTimerManager().SetTimer(HorizontalBounceTimerHandle, this, &ALab_BlockoutStadiumCharacter::OnHorizontalBounceTimer, 2.0f, false);
				}
				else
				{
					CurrentMaterialVelocity.X = CurrentVelocity.X;
					CurrentMaterialVelocity.Y = CurrentVelocity.Y;
				}
			}
		}
		
		MaterialInstance->SetVectorParameterValue("Velocity", CurrentMaterialVelocity);

		PreviousVelocity = CurrentVelocity;
	}
}


float ALab_BlockoutStadiumCharacter::GetBounceSinDamp(const float CurrentTime) const
{
	return FMath::Pow(exp(1), (-4 * CurrentTime)) * (cos(8 * PI * CurrentTime));
}

void ALab_BlockoutStadiumCharacter::OnVerticalBounceTimer()
{
	GetWorld()->GetTimerManager().ClearTimer(VerticalBounceTimerHandle);
	bVerticalBouncing = false;
}

void ALab_BlockoutStadiumCharacter::OnHorizontalBounceTimer()
{
	GetWorld()->GetTimerManager().ClearTimer(HorizontalBounceTimerHandle);
	bHorizontalBouncing = false;
}


void ALab_BlockoutStadiumCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(ALab_BlockoutStadiumCharacter, LastCharacterController);
	DOREPLIFETIME(ALab_BlockoutStadiumCharacter, LastPlayerState);

	DOREPLIFETIME(ALab_BlockoutStadiumCharacter, PreviousState);
	DOREPLIFETIME(ALab_BlockoutStadiumCharacter, CurrentState);
}


void ALab_BlockoutStadiumCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	// movement sounds
	if (GetMovementComponent()->Velocity.Length() > 0.0f)
	{
		if (CurrentState == EPlayerState::STATE_CHARGING)
		{
			StopMoveSounds();
			return;
		}
		
		if (GetMovementComponent()->IsMovingOnGround())
			StartMoveSounds();
	}
	else if (GetMovementComponent()->Velocity.IsNearlyZero() || GetMovementComponent()->IsFalling())
	{
		StopMoveSounds();
	}

	UpdateBodyJiggle(DeltaSeconds);
}


void ALab_BlockoutStadiumCharacter::ReceiveParticleData_Implementation(const TArray<FBasicParticleData>& Data,
	UNiagaraSystem* NiagaraSystem, const FVector& SimulationPositionOffset)
{
	INiagaraParticleCallbackHandler::ReceiveParticleData_Implementation(Data, NiagaraSystem, SimulationPositionOffset);

	if (!HasAuthority())
		return;

	if (SizeModifierComponent->GetCurrentSize() != EActorSize::SIZE_LARGE)
		return;

	for (int i = 0; i < Data.Num(); i++)
	{
		if (Data[i].Velocity.Length() > AcidParticleVelocityThreshold)
		{
			const FVector SpawnLocation = Data[i].Position;
			const FRotator SpawnRotation = this->GetActorRotation();
			
			const float Scale = FMath::Lerp(AcidParticleMinSize, AcidParticleMaxSize,
				UKismetMathLibrary::NormalizeToRange(Data[i].Size, 0.03f, 0.18f));
			
			AcidPatchComponent->SpawnAcidPatch(Scale, SpawnLocation, SpawnRotation, nullptr, GetCurrentColour());
		}
	}
}


void ALab_BlockoutStadiumCharacter::MoveForward(float Value)
{
	ServerMoveForward(Value);
}

void ALab_BlockoutStadiumCharacter::ServerMoveForward_Implementation(float Value)
{
	MulticastMoveForward(Value);
}


void ALab_BlockoutStadiumCharacter::MulticastMoveForward_Implementation(float Value)
{
	if (CurrentState == EPlayerState::STATE_CHARGING || !IsLocallyControlled())
	{
		return;
	}
	
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		
		this->AddMovementInput(Direction, Value);

		// UE_LOG(LogTemp, Warning, TEXT("Starting player move sounds - forward"));
	}
}


void ALab_BlockoutStadiumCharacter::MoveRight(float Value)
{
	ServerMoveRight(Value);
}

void ALab_BlockoutStadiumCharacter::ServerMoveRight_Implementation(float Value)
{
	MulticastMoveRight(Value);
}

void ALab_BlockoutStadiumCharacter::MulticastMoveRight_Implementation(float Value)
{
	if (CurrentState == EPlayerState::STATE_CHARGING || !IsLocallyControlled())
	{
		return;
	}
	
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		
		this->AddMovementInput(Direction, Value);
		
		// UE_LOG(LogTemp, Warning, TEXT("Starting player move sounds - right"));
	}
}


void ALab_BlockoutStadiumCharacter::MouseX(float Value)
{
	ServerMouseX(Value);
}

void ALab_BlockoutStadiumCharacter::ServerMouseX_Implementation(float Value)
{
	ClientMouseX(Value);
}

void ALab_BlockoutStadiumCharacter::ClientMouseX_Implementation(float Value)
{
	AddControllerYawInput(Value);
}


void ALab_BlockoutStadiumCharacter::MouseY(float Value)
{
	ServerMouseY(Value);
}

void ALab_BlockoutStadiumCharacter::ServerMouseY_Implementation(float Value)
{
	ClientMouseY(Value);
}

void ALab_BlockoutStadiumCharacter::ClientMouseY_Implementation(float Value)
{
	AddControllerPitchInput(Value);
}


void ALab_BlockoutStadiumCharacter::TurnAtRate(float Rate)
{
	ServerTurnAtRate(Rate);
}

void ALab_BlockoutStadiumCharacter::ServerTurnAtRate_Implementation(float Rate)
{
	MulticastTurnAtRate(Rate);
}

void ALab_BlockoutStadiumCharacter::MulticastTurnAtRate_Implementation(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}


void ALab_BlockoutStadiumCharacter::LookUpAtRate(float Rate)
{
	ServerLookUpAtRate(Rate);
}

void ALab_BlockoutStadiumCharacter::ServerLookUpAtRate_Implementation(float Rate)
{
	MulticastLookUpAtRate(Rate);
}

void ALab_BlockoutStadiumCharacter::MulticastLookUpAtRate_Implementation(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}


void ALab_BlockoutStadiumCharacter::SetLastCharacterController(APlayerCharacterController* NewPreviousController)
{
	ServerSetLastCharacterController(NewPreviousController);
}

void ALab_BlockoutStadiumCharacter::ServerSetLastCharacterController_Implementation(APlayerCharacterController* NewPreviousController)
{
	LastCharacterController = NewPreviousController;
	LastPlayerState = NewPreviousController->PlayerState;
	SetOwner(LastCharacterController);

	/*if (LastCharacterController)
	{
		UE_LOG(LogTemp, Warning, TEXT("Player %s received new Last Character Controller %s"), *GetActorLabel(),
		*LastCharacterController->GetActorLabel());	
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No Last Character Controller Set"));
	}*/

	GetMovementComponent()->Velocity = PreviousVelocity;
	
	SetTeam(GetCurrentTeam());
	SetColour(GetCurrentTeam());
}


EPlayerTeam ALab_BlockoutStadiumCharacter::GetCurrentTeam() const
{
	if (GameState)
	{
		if (LastCharacterController)
		{
			// UE_LOG(LogTemp, Warning, TEXT("There is GS or LCC"));
			return GameState->GetPlayerTeam(LastCharacterController->PlayerState);
		}

		if (LastPlayerState)
		{
			return GameState->GetPlayerTeam(LastPlayerState);
		}
		
		return GameState->GetPlayerTeam(GetPlayerState());
	}

	/*if (!LastCharacterController)
		UE_LOG(LogTemp, Warning, TEXT("Player has no Last Character Controller"));

	if (!GameState)
		UE_LOG(LogTemp, Warning, TEXT("Player has no Game State"));*/

	return EPlayerTeam::TEAM_NONE;
}

FLinearColor ALab_BlockoutStadiumCharacter::GetCurrentColour() const
{
	switch (GetCurrentTeam())
	{
	case EPlayerTeam::TEAM1:
		return GameState->TeamOneColour;

	case EPlayerTeam::TEAM2:
		return GameState->TeamTwoColour;

	default:
		return GameState->TeamNoneColour;
	}
}


void ALab_BlockoutStadiumCharacter::SetTeam(EPlayerTeam NewTeam)
{
	ServerSetTeam(NewTeam);
}

bool ALab_BlockoutStadiumCharacter::ServerSetTeam_Validate(EPlayerTeam NewTeam)
{
	if (LastCharacterController && GetCurrentTeam() != NewTeam)
		return false;
	
	return true;
}

void ALab_BlockoutStadiumCharacter::ServerSetTeam_Implementation(EPlayerTeam NewTeam)
{
	switch (NewTeam)
	{
	case EPlayerTeam::TEAM_NONE:
		GetCapsuleComponent()->SetCollisionObjectType(ECC_Pawn);
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_TEAM1, ECollisionResponse::ECR_Overlap);
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_TEAM2, ECollisionResponse::ECR_Overlap);
		break;
		
	case EPlayerTeam::TEAM1:
		GetCapsuleComponent()->SetCollisionObjectType(ECC_TEAM1);
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_TEAM1, ECollisionResponse::ECR_Overlap);
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_TEAM2, ECollisionResponse::ECR_Block);
		break;

	case EPlayerTeam::TEAM2:
		GetCapsuleComponent()->SetCollisionObjectType(ECC_TEAM2);
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_TEAM1, ECollisionResponse::ECR_Block);
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_TEAM2, ECollisionResponse::ECR_Overlap);
		break;

	default:
		break;
	}
	
	// UE_LOG(LogTemp, Warning, TEXT("Player %s new team %s"), *GetActorLabel(), *UEnum::GetValueAsString(NewTeam));

	SetColour(NewTeam);
}


void ALab_BlockoutStadiumCharacter::SetColour(EPlayerTeam CurrentTeam)
{
	ServerSetColour(CurrentTeam);
}

void ALab_BlockoutStadiumCharacter::ServerSetColour_Implementation(EPlayerTeam CurrentTeam)
{
	MulticastSetColour(CurrentTeam);
}

void ALab_BlockoutStadiumCharacter::MulticastSetColour_Implementation(EPlayerTeam CurrentTeam)
{
	if (!MaterialInstance)
	{
		// UE_LOG(LogTemp, Warning, TEXT("No Material to be able to net multicast"));
		return;
	}

	MaterialInstance->SetVectorParameterValue("Colour", GetCurrentColour());
	
	// UE_LOG(LogTemp, Warning, TEXT("Colour has been set via multicast: Team %s"), *UEnum::GetValueAsString(CurrentTeam));
}


void ALab_BlockoutStadiumCharacter::SwitchState(EPlayerState NewState)
{
	ServerSwitchState(NewState);
}

void ALab_BlockoutStadiumCharacter::ServerSwitchState_Implementation(EPlayerState NewState)
{
	PreviousState = CurrentState;
	CurrentState = NewState;

	// UE_LOG(LogTemp, Warning, TEXT("Player %s changed state: %s"), *GetActorLabel(), *UEnum::GetValueAsString(CurrentState));

	switch (CurrentState)
	{
	case EPlayerState::STATE_NORMAL:
		break;
		
	case EPlayerState::STATE_AIMING:
		break;
		
	case EPlayerState::STATE_CHARGING:
		AdjustCamera(ChargingCameraDistance, ChargingCameraOffset, true);
		
		StopMoveSounds();
		break;
		
	case EPlayerState::STATE_FLYING:
		break;

	case EPlayerState::STATE_GRABBING:
		break;
		
	default:
		break;
	}

	MulticastSwitchState(NewState);
}

void ALab_BlockoutStadiumCharacter::MulticastSwitchState_Implementation(EPlayerState NewState)
{
	switch (NewState)
	{
	case EPlayerState::STATE_NORMAL:
		SpeedMultiplier = 1.0f;
		break;
		
	case EPlayerState::STATE_AIMING:
		SpeedMultiplier = AimSpeedMultiplier;
		break;
		
	case EPlayerState::STATE_CHARGING:
		break;
		
	case EPlayerState::STATE_FLYING:
		SpeedMultiplier = FlyingSpeedMultiplier;
		break;

	case EPlayerState::STATE_GRABBING:
		SpeedMultiplier = GrabSpeedMultiplier;
		break;
		
	default:
		break;
	}
	
	GetCharacterMovement()->MaxWalkSpeed = MoveSpeeds[static_cast<int>(SizeModifierComponent->GetCurrentSize())] * SpeedMultiplier;
}


void ALab_BlockoutStadiumCharacter::AdjustCamera(float CameraDistance, FVector CameraOffset,
                                                 bool LockCharRotationToCamera)
{
	ServerAdjustCamera(CameraDistance, CameraOffset, LockCharRotationToCamera);
}

void ALab_BlockoutStadiumCharacter::ServerAdjustCamera_Implementation(float CameraDistance, FVector CameraOffset,
                                                                      bool LockCharRotationToCamera)
{
	bUseControllerRotationYaw = LockCharRotationToCamera;
	GetCharacterMovement()->bOrientRotationToMovement = !LockCharRotationToCamera;
	
	ClientAdjustCamera(CameraDistance, CameraOffset, LockCharRotationToCamera);
}


void ALab_BlockoutStadiumCharacter::ClientAdjustCamera_Implementation(float CameraDistance, FVector CameraOffset,
                                                                      bool LockCharRotationToCamera)
{
	GetCameraBoom()->TargetArmLength = CameraDistance;
	GetCameraBoom()->SocketOffset = CameraOffset;
}


void ALab_BlockoutStadiumCharacter::ResetCamera()
{
	AdjustCamera(DefaultCameraDistance, DefaultCameraOffset, false);	
}


void ALab_BlockoutStadiumCharacter::Kick() const
{
	if (CurrentState != EPlayerState::STATE_NORMAL)
		return;

	KickComponent->Kick();
}


void ALab_BlockoutStadiumCharacter::GrabAndThrow() const
{
	GrabThrowComponent->GrabAndThrow();
}


void ALab_BlockoutStadiumCharacter::SpotSplit()
{
	ServerSpotSplit();
}

void ALab_BlockoutStadiumCharacter::ServerSpotSplit_Implementation()
{
	if (CurrentState == EPlayerState::STATE_CHARGING || CurrentState == EPlayerState::STATE_GRABBING)
		return;
	
	FVector SplitDir = GetActorForwardVector() * -1;
	SplitDir = SplitDir.RotateAngleAxis(10.0f, GetActorRightVector());

	CombineSplitComponent->Split(SplitDir, 400.0f);

	if (GetPlayerState())
	{
		if (Cast<ASportPlayerState>(GetPlayerState()))
		{
			Cast<ASportPlayerState>(GetPlayerState())->AddTimesSplit(1);
		}
	}
}


void ALab_BlockoutStadiumCharacter::AimStart()
{
	ServerAimStart();
}

void ALab_BlockoutStadiumCharacter::ServerAimStart_Implementation()
{
	if (CurrentState != EPlayerState::STATE_NORMAL)
		return;
	
	SwitchState(EPlayerState::STATE_AIMING);
	AdjustCamera(AimingCameraDistance, AimingCameraOffset, true);
}


void ALab_BlockoutStadiumCharacter::AimEnd()
{
	ServerAimEnd();
}

void ALab_BlockoutStadiumCharacter::ServerAimEnd_Implementation()
{
	if (CurrentState == EPlayerState::STATE_AIMING)
	{
		SwitchState(EPlayerState::STATE_NORMAL);
		ResetCamera();
	}
}


void ALab_BlockoutStadiumCharacter::ShootSplit()
{
	ServerShootSplit();
}

void ALab_BlockoutStadiumCharacter::ServerShootSplit_Implementation()
{
	if (CurrentState != EPlayerState::STATE_AIMING)
		return;

	const FVector SplitDir = GetFollowCamera()->GetForwardVector();

	CombineSplitComponent->Split(SplitDir, ShootSplitForce);

	if (GetPlayerState())
	{
		if (Cast<ASportPlayerState>(GetPlayerState()))
		{
			Cast<ASportPlayerState>(GetPlayerState())->AddTimesShot(1);
		}
	}
}


void ALab_BlockoutStadiumCharacter::LaunchStart() const
{
	JumpLaunchComponent->LaunchStart();
}

void ALab_BlockoutStadiumCharacter::LaunchEnd() const
{
	JumpLaunchComponent->LaunchEnd();
}


void ALab_BlockoutStadiumCharacter::SpawnCollisionParticles(FVector ColDir)
{
	ServerSpawnCollisionParticles(ColDir);
}

void ALab_BlockoutStadiumCharacter::ServerSpawnCollisionParticles_Implementation(FVector ColDir)
{
	MulticastSpawnCollisionParticles(ColDir);
}

void ALab_BlockoutStadiumCharacter::MulticastSpawnCollisionParticles_Implementation(FVector ColDir)
{
	if (!NS_SlimePieces)
		return;

	FFXSystemSpawnParameters SpawnParameters;

	SpawnParameters.SystemTemplate = NS_SlimePieces;
	SpawnParameters.WorldContextObject = GetWorld();
	SpawnParameters.bAutoActivate = false;
	SpawnParameters.bAutoDestroy = false;
	SpawnParameters.Location = this->GetActorLocation();
	SpawnParameters.Rotation = this->GetActorRotation();

	if (UNiagaraComponent* SlimePiecesComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocationWithParams(SpawnParameters))
	{
		// set particle colour
		SlimePiecesComponent->SetNiagaraVariableLinearColor(FString("Colour"), GetCurrentColour());
		
		SlimePiecesComponent->SetNiagaraVariableVec3(FString("BurstDirection"), ColDir);

		SlimePiecesComponent->SetNiagaraVariableFloat(FString("VelocityScale"), SizeModifierComponent->GetCurrentScale().X);
		SlimePiecesComponent->SetNiagaraVariableInt(FString("BurstAmount"), SlimePiecesAmount[static_cast<int>(SizeModifierComponent->GetCurrentSize())]);

		SlimePiecesComponent->SetVariableObject(FName("User.ClassToTell"), this);

		SlimePiecesComponent->Activate();
	}
	else
	{
		//UE_LOG(LogTemp, Warning, TEXT("Could not spawn slime piece particles"));
	}
}


void ALab_BlockoutStadiumCharacter::PlayOneOffSound(USoundBase* Sound)
{
	if (HasNetOwner())
		ServerPlayOneOffSound(Sound);
}

void ALab_BlockoutStadiumCharacter::ServerPlayOneOffSound_Implementation(USoundBase* Sound)
{
	MulticastPlayOneOffSound(Sound);
}

void ALab_BlockoutStadiumCharacter::MulticastPlayOneOffSound_Implementation(USoundBase* Sound)
{
	if (Sound)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), Sound, GetActorLocation());
	}
}


void ALab_BlockoutStadiumCharacter::PlayCollisionSound(float ColVel)
{
	if (HasNetOwner())
		ServerPlayCollisionSound(ColVel);
}

void ALab_BlockoutStadiumCharacter::ServerPlayCollisionSound_Implementation(float ColVel)
{
	MulticastPlayCollisionSound(ColVel);
}

void ALab_BlockoutStadiumCharacter::MulticastPlayCollisionSound_Implementation(float ColVel)
{
	if (SBCollision)
	{
		float RemapVel =  FMath::Lerp(0.2f, 1.0f,
			UKismetMathLibrary::NormalizeToRange(ColVel, 0.0f, 300.0f));

		RemapVel = FMath::Clamp(RemapVel, 0.0f, 1.0f);
		
		UAudioComponent* SBAudio = UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SBCollision, GetActorLocation());
		SBAudio->SetFloatParameter(FName("CollisionVelNorm"), RemapVel);
		SBAudio->SetTriggerParameter(FName("On Collide"));
	}
}


void ALab_BlockoutStadiumCharacter::StartMoveSounds()
{
	if (HasNetOwner())
		ServerStartMoveSounds();
}

void ALab_BlockoutStadiumCharacter::ServerStartMoveSounds_Implementation()
{
	MulticastStartMoveSounds();
}

void ALab_BlockoutStadiumCharacter::MulticastStartMoveSounds_Implementation()
{
	if (SBMoveAudio && !bIsMoveSoundsPlaying)
	{
		// if (HasAuthority())
		// {
		// 	UE_LOG(LogTemp, Warning, TEXT("Start Player Movement Sounds - Server"));
		// }
		// else
		// {
		// 	UE_LOG(LogTemp, Warning, TEXT("Start Player Movement Sounds - Client"));
		// }
		
		bIsMoveSoundsPlaying = true;
		MoveAudioComponent = UGameplayStatics::SpawnSoundAttached(SBMoveAudio, RootComponent, NAME_None, GetActorLocation(), GetActorRotation(), EAttachLocation::KeepWorldPosition);
	}
}


void ALab_BlockoutStadiumCharacter::StopMoveSounds()
{
	if (HasNetOwner())
		ServerStopMoveSounds();
}

void ALab_BlockoutStadiumCharacter::ServerStopMoveSounds_Implementation()
{
	MulticastStopMoveSounds();
}

void ALab_BlockoutStadiumCharacter::MulticastStopMoveSounds_Implementation()
{
	if (SBMoveAudio && bIsMoveSoundsPlaying && MoveAudioComponent)
	{
		if (bIsMoveSoundsPlaying)
		{
			// if (HasAuthority())
			// {
			// 	UE_LOG(LogTemp, Warning, TEXT("Stop Player Movement Sounds - Server"));
			// }
			// else
			// {
			// 	UE_LOG(LogTemp, Warning, TEXT("Stop Player Movement Sounds - Client"));
			// }
		}
		
		bIsMoveSoundsPlaying = false;
		MoveAudioComponent->SetTriggerParameter(FName("On Stop"));
		MoveAudioComponent->StopDelayed(1.0f);
	}
}

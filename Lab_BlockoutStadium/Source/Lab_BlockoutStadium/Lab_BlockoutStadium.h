// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

// Naming collision channels
#define ECC_TEAM1 ECC_GameTraceChannel1
#define ECC_TEAM2 ECC_GameTraceChannel2
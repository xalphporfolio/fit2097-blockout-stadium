// Fill out your copyright notice in the Description page of Project Settings.


#include "SlimeAcidPatch.h"

#include "SportGameState.h"
#include "Components/DecalComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/AudioComponent.h"


ASlimeAcidPatch::ASlimeAcidPatch()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	NetPriority = 3.0f;

	AcidDamage = 0;
	DefaultCollisionRadius = 160.0f;
	DefaultDecalSize = FVector(120.0f, 160.0f, 160.0f);

	PatchDecal = CreateDefaultSubobject<UDecalComponent>(TEXT("SlimePatchDecal"));
	PatchDecal->SetupAttachment(FieldSystemComponent);
	
	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(FieldSystemComponent);
	SphereCollision->SetIsReplicated(true);
	SphereCollision->SetSphereRadius(DefaultCollisionRadius);

	ReturnResultsTerminal = CreateDefaultSubobject<UReturnResultsTerminal>(TEXT("ReturnResultsTerminal"));
	RadialFalloff = CreateDefaultSubobject<URadialFalloff>(TEXT("RadialFalloff"));
	UniformScalar = CreateDefaultSubobject<UUniformScalar>(TEXT("UniformScalar"));
	OperatorField = CreateDefaultSubobject<UOperatorField>(TEXT("OperatorField"));
	CullingField = CreateDefaultSubobject<UCullingField>(TEXT("CullingField"));
}


void ASlimeAcidPatch::BeginPlay()
{
	Super::BeginPlay();

	if (DecalMaterialClass)
	{
		DecalMaterialInstance = UMaterialInstanceDynamic::Create(DecalMaterialClass, this);
		PatchDecal->SetDecalMaterial(DecalMaterialInstance);
		PatchDecal->DecalSize = DefaultDecalSize;
	}
}


void ASlimeAcidPatch::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (HasAuthority())
	{
		TickDamage();
	}
}


void ASlimeAcidPatch::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASlimeAcidPatch, AcidDamage);
}


void ASlimeAcidPatch::TickDamage()
{
	ServerTickDamage();
}

void ASlimeAcidPatch::ServerTickDamage_Implementation()
{
	MulticastTickDamage();
}

void ASlimeAcidPatch::MulticastTickDamage_Implementation()
{
	UniformScalar->SetUniformScalar(AcidDamage);
	
	OperatorField->SetOperatorField(1.0f, ReturnResultsTerminal, UniformScalar, EFieldOperationType::Field_Substract);

	RadialFalloff->SetRadialFalloff(1.0f, 0.0f, 1.0f, 0.0f, SphereCollision->GetScaledSphereRadius(), SphereCollision->GetComponentLocation(), EFieldFalloffType::Field_FallOff_None);

	CullingField->SetCullingField(RadialFalloff, OperatorField, EFieldCullingOperationType::Field_Culling_Outside);
	
	FieldSystemComponent->ApplyPhysicsField(true, EFieldPhysicsType::Field_InternalClusterStrain, nullptr, CullingField);
	
	/*if (HasAuthority())
	{
		UE_LOG(LogTemp, Warning, TEXT("Ticking Damage on Acid Patch (Server)"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Ticking Damage on Acid Patch (Client)"));
	}*/
}


void ASlimeAcidPatch::SetAcidPatch(const float Scale, const FLinearColor Colour, const float Rotation, const float AcidDuration,
		const float AcidTickDamage)
{
	ServerSetAcidPatch(Scale, Colour, Rotation, AcidDuration, AcidTickDamage);
}

bool ASlimeAcidPatch::ServerSetAcidPatch_Validate(const float Scale, const FLinearColor Colour, const float Rotation, const float AcidDuration,
	const float AcidTickDamage)
{
	if ((Scale > 0.0f && Scale < 5.0f) && AcidTickDamage < 1000000)
	{
		return true;
	}

	return false;
}

void ASlimeAcidPatch::ServerSetAcidPatch_Implementation(const float Scale, const FLinearColor Colour, const float Rotation, const float AcidDuration,
                                                        const float AcidTickDamage)
{
	AcidDamage = AcidTickDamage;
	
	// set rotation
	FRotator PatchRotation = GetActorRotation();
	PatchRotation.Yaw = Rotation;

	SetActorRotation(PatchRotation);

	SphereCollision->SetRelativeScale3D(Scale * FVector::One());
	
	// set decal
	SetDecal(Scale, Colour, Rotation);

	// play audio
	PlaySound(Scale);

	GetWorld()->GetTimerManager().SetTimer(AcidDurationTimerHandle, this, &ASlimeAcidPatch::OnAcidDurationTimer, AcidDuration, false);

	// UE_LOG(LogTemp, Warning, TEXT("Acid Patch Configured"));
}


void ASlimeAcidPatch::SetDecal(float Scale, FLinearColor Colour, float DecalRotation)
{
	ServerSetDecal(Scale, Colour, DecalRotation);
}


void ASlimeAcidPatch::ServerSetDecal_Implementation(float Scale, FLinearColor Colour, float DecalRotation)
{
	MulticastSetDecal(Scale, Colour, DecalRotation);
}

void ASlimeAcidPatch::MulticastSetDecal_Implementation(float Scale, FLinearColor Colour, float DecalRotation)
{
	if (DecalMaterialInstance)
	{
		DecalMaterialInstance->SetVectorParameterValue("AcidColour", Colour);	
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No Decal Material Instance for Acid Patch"));
	}

	// set scale
	FVector ScaledDecal = PatchDecal->GetComponentScale();
	ScaledDecal.Y *= Scale;
	ScaledDecal.Z *= Scale;
	
	PatchDecal->SetRelativeScale3D(ScaledDecal);
}


void ASlimeAcidPatch::PlaySound(float Scale)
{
	ServerPlaySound(Scale);
}

void ASlimeAcidPatch::ServerPlaySound_Implementation(float Scale)
{
	MulticastPlaySound(Scale);
}

void ASlimeAcidPatch::MulticastPlaySound_Implementation(float Scale)
{
	if (SBOnPlace)
	{
		UAudioComponent* SBAudio = UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SBOnPlace, GetActorLocation());
		SBAudio->SetFloatParameter(FName("PitchScale"), Scale);
		SBAudio->SetTriggerParameter(FName("On Trigger"));
	}
}


void ASlimeAcidPatch::OnAcidDurationTimer()
{
	GetWorld()->GetTimerManager().ClearTimer(AcidDurationTimerHandle);
	Destroy();
}

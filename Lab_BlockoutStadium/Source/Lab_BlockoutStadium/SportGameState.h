// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "Net/UnrealNetwork.h"
#include "SportPlayerState.h"
#include "SportGameState.generated.h"


// delegates
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameEnd);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRoundReset);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTeamOneScored);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTeamTwoScored);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTeamOneScoreChanged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTeamTwoScoreChanged);


UENUM(BlueprintType)
enum class EPlayerTeam:uint8
{
	TEAM_NONE UMETA(DisplayName = "No Team"),	
	TEAM1 UMETA(DisplayName = "Team1"),
	TEAM2 UMETA(DisplayName = "Team2"),
};


UCLASS()
class LAB_BLOCKOUTSTADIUM_API ASportGameState : public AGameStateBase
{
	GENERATED_BODY()


public:
	ASportGameState();
	
	// delegates
	UPROPERTY(BlueprintAssignable)
	FOnGameStart OnGameStart;
	UPROPERTY(BlueprintAssignable)
	FOnGameEnd OnGameEnd;

	UPROPERTY(BlueprintAssignable)
	FOnRoundReset OnRoundReset;

	UPROPERTY(BlueprintAssignable)
	FOnTeamOneScoreChanged OnTeamOneScored;
	UPROPERTY(BlueprintAssignable)
	FOnTeamTwoScoreChanged OnTeamTwoScored;

	UPROPERTY(BlueprintAssignable)
	FOnTeamOneScoreChanged OnTeamOneScoreChanged;
	UPROPERTY(BlueprintAssignable)
	FOnTeamTwoScoreChanged OnTeamTwoScoreChanged;

	// properties
	UPROPERTY(EditAnywhere)
	bool bIsMultiplayer;
	
	UPROPERTY(ReplicatedUsing = OnRep_GameStarted)
	bool bGameStarted;

	FTimerHandle GameStartDelayHandle;

	FTimerHandle GameTimerHandle;
	UPROPERTY(EditDefaultsOnly)
	float RoundTimeSeconds;
	UPROPERTY(Replicated)
	float CurrentRoundTimeRemaining;

	FTimerHandle GameScoreDelayHandle;
	UPROPERTY(EditDefaultsOnly)
	float ScoreDelayTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<class ALab_BlockoutStadiumCharacter> PlayerCharacter;
	
	FVector TeamOneSpawnPos;
	FRotator TeamOneSpawnRot;
	FVector TeamTwoSpawnPos;
	FRotator TeamTwoSpawnRot;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<class ABall> BallClass;
	
	FVector BallSpawnPos;
	FRotator BallSpawnRot;
	
	UPROPERTY(EditAnywhere, Replicated)
	TArray<ASportPlayerState*> TeamOne;
	UPROPERTY(EditAnywhere, Replicated)
	TArray<ASportPlayerState*> TeamTwo;

	UPROPERTY(EditAnywhere)
	FLinearColor TeamNoneColour;
	UPROPERTY(EditAnywhere)
	FLinearColor TeamOneColour;
	UPROPERTY(EditAnywhere)
	FLinearColor TeamTwoColour;
	
	UPROPERTY(ReplicatedUsing = OnRep_SetTeamOneScore)
	int TeamOneScore;
	UPROPERTY(ReplicatedUsing = OnRep_SetTeamTwoScore)
	int TeamTwoScore;

	
protected:
	UPROPERTY(Replicated)
	ABall* CurrentBall;
	

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnBallDestroyed(AActor* DestroyedActor);
	

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void Tick(float DeltaSeconds) override;

	virtual void AddPlayerState(APlayerState* PlayerState) override;

	void SetUpPlayer(APlayerState* PlayerState);
	UFUNCTION(Server, Reliable)
	void ServerSetUpPlayer(APlayerState* PlayerState);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastSetUpPlayer(APlayerState* PlayerState);
	
	ALab_BlockoutStadiumCharacter* SpawnNewPlayer(const EPlayerTeam TargetTeam) const;

	EPlayerTeam GetPlayerTeam(APlayerState* PlayerState) const;

	ABall* SpawnNewBall();

	void StartGame();
	UFUNCTION(Server, Reliable)
	void ServerStartGame();

	UFUNCTION()
	void OnGameStartDelay();
	
	void GoalScored();
	UFUNCTION(Server, Reliable)
	void ServerGoalScored();

	UFUNCTION()
	void OnGameScoreDelay();

	void ResetRound();
	UFUNCTION(Server, Reliable)
	void ServerResetRound();

	void StopGame();
	UFUNCTION(Server, Reliable)
	void ServerStopGame();

	UFUNCTION()
	void OnGameTimerEnd();
	
	UFUNCTION()
	void OnRep_GameStarted();
	
	void TeamOneGoalScored(int Amount);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerTeamOneGoalScored(int Amount);
	UFUNCTION()
	void OnRep_SetTeamOneScore();
	
	void TeamTwoGoalScored(int Amount);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerTeamTwoGoalScored(int Amount);
	UFUNCTION()
	void OnRep_SetTeamTwoScore();
};

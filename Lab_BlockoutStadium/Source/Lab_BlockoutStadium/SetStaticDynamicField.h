// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "Field/FieldSystemActor.h"
#include "SetStaticDynamicField.generated.h"

/**
 * 
 */
UCLASS()
class LAB_BLOCKOUTSTADIUM_API ASetStaticDynamicField : public AFieldSystemActor
{
	GENERATED_BODY()

public:
	ASetStaticDynamicField();

	UPROPERTY(EditAnywhere)
	UBoxComponent* BoxCollision;

	UPROPERTY(EditAnywhere)
	UBoxFalloff* BoxFalloff;

	UPROPERTY(EditAnywhere)
	UUniformInteger* UniformInteger;

	UPROPERTY(EditAnywhere)
	UCullingField* CullingField;

	
public:
	void CreateField(int ChaosState, FVector FieldSize);
	
};

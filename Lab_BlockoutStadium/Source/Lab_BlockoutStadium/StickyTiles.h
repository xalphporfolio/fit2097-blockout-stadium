// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "Ball.h"
#include "StickyTiles.generated.h"


UCLASS()
class LAB_BLOCKOUTSTADIUM_API AStickyTiles : public AActor
{
	GENERATED_BODY()

	
public:	
	AStickyTiles();

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* TileMesh;

	UPROPERTY(EditDefaultsOnly)
	UBoxComponent* HitBox;

	UPROPERTY(EditDefaultsOnly)
	float VelocityDampFactor;

	
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnHitBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};

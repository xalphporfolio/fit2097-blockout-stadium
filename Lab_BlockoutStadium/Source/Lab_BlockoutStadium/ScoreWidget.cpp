// Fill out your copyright notice in the Description page of Project Settings.


#include "ScoreWidget.h"


void UScoreWidget::NativeConstruct()
{
	Super::NativeConstruct();

	GameState = Cast<ASportGameState>(GetWorld()->GetGameState());

	if (GameState)
	{
		GameState->OnTeamOneScoreChanged.AddDynamic(this, &UScoreWidget::OnTeamOneScoreChanged);
		GameState->OnTeamTwoScoreChanged.AddDynamic(this, &UScoreWidget::OnTeamTwoScoreChanged);

		// UE_LOG(LogTemp, Warning, TEXT("Game State Delegates Set"));
	}
}

void UScoreWidget::OnTeamOneScoreChanged()
{
	TeamOneScore->SetText(FText::AsNumber(GameState->TeamOneScore));
}


void UScoreWidget::OnTeamTwoScoreChanged()
{
	TeamTwoScore->SetText(FText::AsNumber(GameState->TeamTwoScore));
}
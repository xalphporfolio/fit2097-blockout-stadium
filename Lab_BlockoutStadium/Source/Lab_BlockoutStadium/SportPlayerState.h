// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "Net/UnrealNetwork.h"
#include "SportPlayerState.generated.h"


UCLASS()
class LAB_BLOCKOUTSTADIUM_API ASportPlayerState : public APlayerState
{
	GENERATED_BODY()

	
protected:
	virtual void ClientInitialize(AController* C) override;


public:
	ASportPlayerState();

	UPROPERTY(Replicated)
	FString OnFieldName;

	// for player stats
	UPROPERTY(Replicated)
	int Kicks;
	UPROPERTY(Replicated)
	int BallGrabs;
	UPROPERTY(Replicated)
	int BallThrows;
	UPROPERTY(Replicated)
	int GoalsScored;
	UPROPERTY(Replicated)
	int TimesSplit;
	UPROPERTY(Replicated)
	int TimesShot;
	UPROPERTY(Replicated)
	int TimesLaunched;
	UPROPERTY(Replicated)
	int TimesKnockedBallUp;
	UPROPERTY(Replicated)
	int TimesKnockedBallForward;
	

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void SetFieldName(const FString& NewName);
	UFUNCTION(Server, Reliable)
	void ServerSetFieldName(const FString& NewName);
	
	void AddKicks(const int Amount);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAddKicks(const int Amount);
	
	void AddBallGrabs(const int Amount);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAddBallGrabs(const int Amount);

	void AddBallThrows(const int Amount);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAddBallThrows(const int Amount);

	void AddGoals(const int Amount);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAddGoals(const int Amount);

	void AddTimesSplit(const int Amount);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerTimeSplit(const int Amount);

	void AddTimesShot(const int Amount);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAddTimesShot(const int Amount);

	void AddTimesLaunched(const int Amount);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAddTimesLaunched(const int Amount);

	void AddTimesKnockedBallUp(const int Amount);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAddTimesKnockedBallUp(const int Amount);

	void AddTimesKnockedBallForward(const int Amount);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAddTimesKnockedBallForward(const int Amount);
};
// Fill out your copyright notice in the Description page of Project Settings.


#include "WreckingBall.h"


AWreckingBall::AWreckingBall()
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	Top = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Top"));
	Ball = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Ball"));
	PhysicsConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("Physics Constraint"));
	Rope = CreateDefaultSubobject<UCableComponent>(TEXT("Rope"));

	Top->SetupAttachment(RootComponent);
	Ball->SetupAttachment(RootComponent);
	PhysicsConstraint->SetupAttachment(RootComponent);
	Rope->SetupAttachment(RootComponent);
}

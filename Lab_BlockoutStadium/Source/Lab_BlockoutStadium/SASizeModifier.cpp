// Fill out your copyright notice in the Description page of Project Settings.


#include "SASizeModifier.h"
#include "Net/UnrealNetwork.h"


USASizeModifier::USASizeModifier()
{
	PrimaryComponentTick.bCanEverTick = false;

	CurrentSize = EActorSize::SIZE_LARGE;
	SizeScales = {FVector::One(), FVector::One() * 0.75f, FVector::One() * 0.45f};
}


void USASizeModifier::BeginPlay()
{
	Super::BeginPlay();
	GetOwner()->SetActorScale3D(SizeScales[static_cast<int>(CurrentSize)]);
}


void USASizeModifier::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USASizeModifier, CurrentSize);
}


void USASizeModifier::SetSize(const EActorSize NewSize)
{
	ServerSetSize(NewSize);
}

void USASizeModifier::ServerSetSize_Implementation(const EActorSize NewSize)
{
	CurrentSize = NewSize;

	//UE_LOG(LogTemp, Warning, TEXT("Player %s Current Size %s (Server)"), *GetOwner()->GetActorNameOrLabel(), *UEnum::GetValueAsString(CurrentSize));
	
	MulticastSetSize(NewSize);
}

void USASizeModifier::MulticastSetSize_Implementation(const EActorSize NewSize)
{
	GetOwner()->SetActorScale3D(SizeScales[static_cast<int>(NewSize)]);
	
	OnSizeChanged.Broadcast(NewSize);
}


void USASizeModifier::ReduceSize()
{
	switch (CurrentSize)
	{
	case EActorSize::SIZE_LARGE:
		SetSize(EActorSize::SIZE_MID);
		break;
		
	case EActorSize::SIZE_MID:
		SetSize(EActorSize::SIZE_SMALL);
		break;
		
	default:
		break;
	}
}


void USASizeModifier::IncreaseSize()
{
	switch (CurrentSize)
	{
	case EActorSize::SIZE_SMALL:
		SetSize(EActorSize::SIZE_MID);
		break;
		
	case EActorSize::SIZE_MID:
		SetSize(EActorSize::SIZE_LARGE);
		break;
		
	default:
		break;
	}
}


EActorSize USASizeModifier::GetCurrentSize() const
{
	return CurrentSize;
}

FVector USASizeModifier::GetCurrentScale() const
{
	return SizeScales[static_cast<int>(CurrentSize)];
}

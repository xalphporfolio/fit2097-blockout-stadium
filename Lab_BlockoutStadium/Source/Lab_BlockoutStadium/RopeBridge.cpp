// Fill out your copyright notice in the Description page of Project Settings.


#include "RopeBridge.h"


// Sets default values
ARopeBridge::ARopeBridge()
{
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	// constraints
	FrontEndConstraintRight = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("FrontEndConstraintRight"));
	FrontEndConstraintRight->SetupAttachment(RootComponent);
	FrontEndConstraintLeft = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("FrontEndConstraintLeft"));
	FrontEndConstraintLeft->SetupAttachment(RootComponent);
	
	RightConstraint1 = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("RightConstraint1"));
	RightConstraint1->SetupAttachment(RootComponent);
	RightConstraint2 = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("RightConstraint2"));
	RightConstraint2->SetupAttachment(RootComponent);
	RightConstraint3 = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("RightConstraint3"));
	RightConstraint3->SetupAttachment(RootComponent);
	RightConstraint4 = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("RightConstraint4"));
	RightConstraint4->SetupAttachment(RootComponent);

	LeftConstraint1 = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("LeftConstraint1"));
	LeftConstraint1->SetupAttachment(RootComponent);
	LeftConstraint2 = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("LeftConstraint2"));
	LeftConstraint2->SetupAttachment(RootComponent);
	LeftConstraint3 = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("LeftConstraint3"));
	LeftConstraint3->SetupAttachment(RootComponent);
	LeftConstraint4 = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("LeftConstraint4"));
	LeftConstraint4->SetupAttachment(RootComponent);

	BackEndConstraintRight = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("BackEndConstraintRight"));
	BackEndConstraintRight->SetupAttachment(RootComponent);
	BackEndConstraintLeft = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("BackEndConstraintLeft"));
	BackEndConstraintLeft->SetupAttachment(RootComponent);

	// planks
	Plank1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plank1"));
	Plank1->SetupAttachment(RootComponent);
	Plank2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plank2"));
	Plank2->SetupAttachment(RootComponent);
	Plank3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plank3"));
	Plank3->SetupAttachment(RootComponent);
	Plank4 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plank4"));
	Plank4->SetupAttachment(RootComponent);
	Plank5 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plank5"));
	Plank5->SetupAttachment(RootComponent);
}

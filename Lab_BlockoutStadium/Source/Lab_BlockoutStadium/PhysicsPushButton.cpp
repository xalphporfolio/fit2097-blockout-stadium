// Fill out your copyright notice in the Description page of Project Settings.


#include "PhysicsPushButton.h"


APhysicsPushButton::APhysicsPushButton()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	NetPriority = 3.0f;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	ButtonBaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ButtonBaseMesh"));
	ButtonBaseMesh->SetupAttachment(RootComponent);

	ButtonMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ButtonMesh"));
	ButtonMesh->SetupAttachment(RootComponent);

	ButtonConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("ButtonConstraint"));
	ButtonConstraint->SetupAttachment(RootComponent);

	ButtonSensitivity = 15.0f;

	bOneTimeUse = true;
}


void APhysicsPushButton::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority())
	{
		const float ButtonDepression = (ButtonMesh->GetComponentLocation() - ButtonBaseMesh->GetComponentLocation()).Z;
		if (ButtonDepression <= ButtonSensitivity)
		{
			OnButtonPressed.Broadcast();
			// UE_LOG(LogTemp, Warning, TEXT("Button Pressed"));

			if (bOneTimeUse)
			{
				Destroy();
			}
		}
	}
}


// Fill out your copyright notice in the Description page of Project Settings.


#include "ReactiveLightStrip.h"

AReactiveLightStrip::AReactiveLightStrip()
{
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	
	LightBase = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Light Base"));
	LightBase->SetupAttachment(RootComponent);

	Light = CreateDefaultSubobject<URectLightComponent>(TEXT("Light"));
	Light->SetupAttachment(LightBase);
}


void AReactiveLightStrip::BeginPlay()
{
	Super::BeginPlay();

	GameState = Cast<ASportGameState>(GetWorld()->GetGameState());

	if (GameState)
	{
		GameState->OnGameStart.AddDynamic(this, &AReactiveLightStrip::OnGameStart);
		GameState->OnRoundReset.AddDynamic(this, &AReactiveLightStrip::OnRoundReset);
		GameState->OnTeamOneScored.AddDynamic(this, &AReactiveLightStrip::OnTeamOneScored);
		GameState->OnTeamTwoScored.AddDynamic(this, &AReactiveLightStrip::OnTeamTwoScored);

		Light->SetLightColor(GameState->TeamOneColour);
	}
	else
	{
		// UE_LOG(LogTemp, Warning, TEXT("Reactive Light Could not get Game State"));
	}
}


void AReactiveLightStrip::OnGameStart()
{
	Light->SetLightColor(GameState->TeamNoneColour);
}


void AReactiveLightStrip::OnRoundReset()
{
	Light->SetLightColor(GameState->TeamNoneColour);
}


void AReactiveLightStrip::OnTeamOneScored()
{
	Light->SetLightColor(GameState->TeamOneColour);

	// UE_LOG(LogTemp, Warning, TEXT("Colour lights: Team 1"));
}

void AReactiveLightStrip::OnTeamTwoScored()
{
	Light->SetLightColor(GameState->TeamTwoColour);

	// UE_LOG(LogTemp, Warning, TEXT("Colour lights: Team 2"));
}

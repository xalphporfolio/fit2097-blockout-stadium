// Fill out your copyright notice in the Description page of Project Settings.


#include "SAJumpLaunch.h"
#include "Lab_BlockoutStadiumCharacter.h"
#include "NiagaraFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/AudioComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"


USAJumpLaunch::USAJumpLaunch()
{
	PrimaryComponentTick.bCanEverTick = true;

	bIsCharging = false;
	CurrentChargeTime = 0.0f;

	JumpSpeeds = {500.0f, 700.0f, 900.0f};

	LaunchForces = {1000.0f, 1200.0f, 1400.0f};

	MinChargeTime = 0.5f;
	MaxChargeTime = 2.0f;

	LaunchAngleOffset = FRotator(40.0f, 0.0f,0.0f);
	MaxLaunchPitch = 30.0f;

	bIsChargeSoundsPlaying = false;
	bIsMaxChargeSoundsPlaying = false;
}


void USAJumpLaunch::InitComponent(ALab_BlockoutStadiumCharacter* Player, USASizeModifier* SizeModifier)
{
	PlayerRef = Player;
	SizeModifierCompRef = SizeModifier;

	if (!PlayerRef)
		UE_LOG(LogTemp, Warning, TEXT("JumpLaunch Component - No PlayerRef received"));

	if (!SizeModifierCompRef)
		UE_LOG(LogTemp, Warning, TEXT("JumpLaunch Component - No SizeModifierCompRef received"));

	if (!PlayerRef || !SizeModifierCompRef)
		return;

	PlayerRef->GetCharacterMovement()->JumpZVelocity = JumpSpeeds[static_cast<int>(SizeModifierCompRef->GetCurrentSize())];
	SizeModifierCompRef->OnSizeChanged.AddDynamic(this, &USAJumpLaunch::OnSizeChanged);
}


void USAJumpLaunch::OnSizeChanged(const EActorSize NewSize)
{
	PlayerRef->GetCharacterMovement()->JumpZVelocity = JumpSpeeds[static_cast<int>(NewSize)];
}


void USAJumpLaunch::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USAJumpLaunch, bIsCharging);
	DOREPLIFETIME(USAJumpLaunch, CurrentChargeTime);
}


void USAJumpLaunch::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PlayerRef)
		return;

	if (PlayerRef->HasAuthority())
	{
		if (PlayerRef->CurrentState == EPlayerState::STATE_NORMAL || PlayerRef->CurrentState == EPlayerState::STATE_CHARGING)
		{
			if (bIsCharging)
			{
				CurrentChargeTime += DeltaTime;
			
				if (CurrentChargeTime > MinChargeTime && (PlayerRef->CurrentState != EPlayerState::STATE_CHARGING))
				{
					PlayerRef->SwitchState(EPlayerState::STATE_CHARGING);
					
					ChargingParticles(false);
					StartChargeSounds(false);
					
					OnChargeStart.Broadcast();
				}

				if (CurrentChargeTime >= MaxChargeTime)
				{
					ChargingParticles(true);
					StartChargeSounds(true);
					
					OnChargeMax.Broadcast();
				}
			}		
		}
	}
}


bool USAJumpLaunch::IsCharging() const
{
	return bIsCharging;
}


void USAJumpLaunch::LaunchStart()
{
	ServerLaunchStart();
}

void USAJumpLaunch::ServerLaunchStart_Implementation()
{
	CurrentChargeTime = 0.0f;
	bIsCharging = true;
}


void USAJumpLaunch::LaunchEnd()
{
	ServerLaunchEnd();
}

void USAJumpLaunch::ServerLaunchEnd_Implementation()
{
	bIsCharging = false;
	
	MulticastLaunchEnd();
}

void USAJumpLaunch::MulticastLaunchEnd_Implementation()
{
	OnChargeEnd.Broadcast();

	if (ChargeParticles)
	{
		ChargeParticles->Deactivate();
	}

	StopChargeSounds();

	if (PlayerRef->CurrentState == EPlayerState::STATE_CHARGING)
	{
		const float ChargeProportion = FMath::Clamp(CurrentChargeTime / MaxChargeTime, 0.0f, 1.0f);
		const float LaunchForce = LaunchForces[static_cast<int>(SizeModifierCompRef->GetCurrentSize())] * ChargeProportion;

		const FRotator CameraAngle = PlayerRef->GetFollowCamera()->GetForwardVector().Rotation();
		const FVector OffsetDir = LaunchAngleOffset.RotateVector(FVector::XAxisVector);
		
		FVector LaunchDir = CameraAngle.RotateVector(OffsetDir);

		// stop player launching backwards when camera is pointed to the sky
		if (FVector::DotProduct(PlayerRef->GetActorForwardVector(), LaunchDir) < 0)
		{
			LaunchDir = FVector::UpVector;
		}

		// UE_LOG(LogTemp, Warning, TEXT("Launch Angle: %s"), *LaunchDir.Rotation().ToString());
		// UE_LOG(LogTemp, Warning, TEXT("Launch Dir: %s"), *LaunchDir.ToString());
		
		PlayerRef->GetCharacterMovement()->AddImpulse(LaunchDir * LaunchForce, true);

		if (PlayerRef->HasNetOwner())
		{
			PlayerRef->SwitchState(EPlayerState::STATE_FLYING);

			if (PlayerRef->IsLocallyControlled())
			{
				PlayLaunchSound();
			}
			
			OnCharacterLaunched.Broadcast();
		}
	}
	else
	{
		PlayerRef->Jump();
		
		OnCharacterJump.Broadcast();
	}
}


void USAJumpLaunch::CancelCharge()
{
	ServerCancelCharge();
}

void USAJumpLaunch::ServerCancelCharge_Implementation()
{
	bIsCharging = false;

	MulticastCancelCharge();
}

void USAJumpLaunch::MulticastCancelCharge_Implementation()
{
	if (ChargeParticles)
	{
		ChargeParticles->Deactivate();
	}

	StopChargeSounds();
}


void USAJumpLaunch::ChargingParticles(bool IsMaxCharge)
{
	ServerChargingParticles(IsMaxCharge);
}

void USAJumpLaunch::ServerChargingParticles_Implementation(bool IsMaxCharge)
{
	MulticastChargingParticles(IsMaxCharge);
}

void USAJumpLaunch::MulticastChargingParticles_Implementation(bool IsMaxCharge)
{
	if (!ChargeParticles)
	{
		FFXSystemSpawnParameters SpawnParameters;

		SpawnParameters.SystemTemplate = NSCharge;
		SpawnParameters.AttachToComponent = PlayerRef->GetRootComponent();
		SpawnParameters.WorldContextObject = GetWorld();
		SpawnParameters.Location = PlayerRef->GetActorLocation() + FVector(0, 0, -60);
		SpawnParameters.Rotation = PlayerRef->GetActorRotation();
		SpawnParameters.LocationType = EAttachLocation::KeepWorldPosition;
		SpawnParameters.bAutoActivate = false;
		SpawnParameters.bAutoDestroy = false;
		
		ChargeParticles = UNiagaraFunctionLibrary::SpawnSystemAttachedWithParams(SpawnParameters);	
	}
	
	if (IsMaxCharge)
	{
		ChargeParticles->SetNiagaraVariableInt(FString("ChargedState"), 1);
	}
	else
	{
		ChargeParticles->SetNiagaraVariableInt(FString("ChargedState"), 0);
		ChargeParticles->SetNiagaraVariableFloat(FString("RingScale"),SizeModifierCompRef->GetCurrentScale().X);
		
		ChargeParticles->Activate();
	}
}


void USAJumpLaunch::StartChargeSounds(bool bIsMaxCharge)
{
	if (PlayerRef->HasNetOwner())
		ServerStartChargeSounds(bIsMaxCharge);
}

void USAJumpLaunch::ServerStartChargeSounds_Implementation(bool bIsMaxCharge)
{
	MulticastStartChargeSounds(bIsMaxCharge);
}

void USAJumpLaunch::MulticastStartChargeSounds_Implementation(bool bIsMaxCharge)
{
	if (bIsMaxCharge && !bIsMaxChargeSoundsPlaying && ChargeAudioComponent)
	{
		bIsMaxChargeSoundsPlaying = true;
		ChargeAudioComponent->SetTriggerParameter(FName("On Max Charge"));
	}
	else if (!bIsChargeSoundsPlaying)
	{
		bIsChargeSoundsPlaying = true;
		ChargeAudioComponent = UGameplayStatics::SpawnSoundAttached(SBCharge, PlayerRef->GetRootComponent(), NAME_None,
			PlayerRef->GetActorLocation(), PlayerRef->GetActorRotation(), EAttachLocation::KeepWorldPosition);
		
		ChargeAudioComponent->SetIntParameter(FName("CurrentSize"), static_cast<int>(SizeModifierCompRef->GetCurrentSize()));
		ChargeAudioComponent->SetFloatParameter(FName("MaxChargeTime"), MaxChargeTime);
		
		ChargeAudioComponent->Play();
	}
}


void USAJumpLaunch::StopChargeSounds()
{
	if (PlayerRef->HasNetOwner())
		ServerStopChargeSounds();
}

void USAJumpLaunch::ServerStopChargeSounds_Implementation()
{
	MulticastStopChargeSounds();
}

void USAJumpLaunch::MulticastStopChargeSounds_Implementation()
{
	if (ChargeAudioComponent && bIsChargeSoundsPlaying)
	{
		bIsChargeSoundsPlaying = false;
		bIsMaxChargeSoundsPlaying = false;
		
		ChargeAudioComponent->SetTriggerParameter(FName("On Stop"));
		ChargeAudioComponent->StopDelayed(10.0f);
	}
}


void USAJumpLaunch::PlayLaunchSound()
{
	if (GetOwner()->HasNetOwner())
		ServerPlayLaunchSound();
}

void USAJumpLaunch::ServerPlayLaunchSound_Implementation()
{
	MulticastPlayLaunchSound();
}

void USAJumpLaunch::MulticastPlayLaunchSound_Implementation()
{
	if (SBLaunch)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SBLaunch, PlayerRef->GetActorLocation());
	}
}

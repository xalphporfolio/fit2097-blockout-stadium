// Fill out your copyright notice in the Description page of Project Settings.


#include "SwingingCentrePlatform.h"

// Sets default values
ASwingingCentrePlatform::ASwingingCentrePlatform()
{
	PrimaryActorTick.bCanEverTick = false;
	
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	// constraints
	RightConstraintHinge = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("RightJointHinge"));
	RightConstraintHinge->SetupAttachment(RootComponent);
	
	LeftConstraintHinge = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("LeftJointHinge"));
	LeftConstraintHinge->SetupAttachment(RootComponent);

	RightConstraintBase = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("RightJointBase"));
	RightConstraintBase->SetupAttachment(RootComponent);
	
	LeftConstraintBase = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("LeftJointBase"));
	LeftConstraintBase->SetupAttachment(RootComponent);

	// meshes 
	RightBase = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RightBase"));
	RightBase->SetupAttachment(RootComponent);

	LeftBase = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LeftBase"));
	LeftBase->SetupAttachment(RootComponent);
	
	Platform = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Platform"));
	Platform->SetupAttachment(RootComponent);
	
	RightPole = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RightPole"));
	RightPole->SetupAttachment(RootComponent);

	LeftPole = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LeftPole"));
	LeftPole->SetupAttachment(RootComponent);

	Platform->SetSimulatePhysics(true);
	RightPole->SetSimulatePhysics(true);
	LeftPole->SetSimulatePhysics(true);
}

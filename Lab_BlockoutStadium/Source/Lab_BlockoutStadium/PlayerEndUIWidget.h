// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "PlayerEndUIWidget.generated.h"

/**
 * 
 */
UCLASS()
class LAB_BLOCKOUTSTADIUM_API UPlayerEndUIWidget : public UUserWidget
{
	GENERATED_BODY()

	
protected:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* FieldNameText;

	// player stats
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* KicksText;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* BallGrabsText;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* BallThrowsText;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* GoalsScoredText;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TimesSplitText;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TimeShotText;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TimesLaunchedText;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TimesKnockedUpText;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TimesKnockedForwardText;

public:
	virtual void NativeConstruct() override;
	
};

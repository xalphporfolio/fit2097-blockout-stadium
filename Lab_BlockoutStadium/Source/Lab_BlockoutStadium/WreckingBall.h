// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "CableComponent.h"
#include "WreckingBall.generated.h"


UCLASS()
class LAB_BLOCKOUTSTADIUM_API AWreckingBall : public AActor
{
	GENERATED_BODY()
	
public:	
	AWreckingBall();

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Top;
	
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Ball;

	UPROPERTY(EditAnywhere)
	UPhysicsConstraintComponent* PhysicsConstraint;

	UPROPERTY(EditAnywhere)
	UCableComponent* Rope;

};

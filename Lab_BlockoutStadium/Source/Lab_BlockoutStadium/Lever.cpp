// Fill out your copyright notice in the Description page of Project Settings.


#include "Lever.h"

// Sets default values
ALever::ALever()
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	Arm = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Lever Arm"));
	Base = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Lever Base"));

	PhysicsConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(TEXT("Physics Constratin"));

	Arm->SetupAttachment(RootComponent);
	Base->SetupAttachment(RootComponent);
	PhysicsConstraint->SetupAttachment(RootComponent);

	Arm->SetSimulatePhysics(true);
}


void ALever::BeginPlay()
{
	Super::BeginPlay();
	
}


void ALever::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Arm->GetRelativeRotation().Pitch >= 45)
	{
		if (WreckingBall)
		{
			WreckingBall->Ball->SetEnableGravity(true);
		}
	}
	else if (Arm->GetRelativeRotation().Pitch <= -45)
	{
		// UE_LOG(LogTemp, Warning, TEXT("Off!"));
	}
}


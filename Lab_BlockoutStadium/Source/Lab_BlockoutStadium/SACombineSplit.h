// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SASizeModifier.h"
#include "Components/CapsuleComponent.h"
#include "SACombineSplit.generated.h"

class ALab_BlockoutStadiumCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCombined);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSplit);


UCLASS()
class LAB_BLOCKOUTSTADIUM_API USACombineSplit : public UCapsuleComponent
{
	GENERATED_BODY()


private:
	UPROPERTY()
	ALab_BlockoutStadiumCharacter* PlayerRef;
	UPROPERTY()
	USASizeModifier* SizeModifierCompRef;
	
	FTimerHandle CanRecombineTimerHandle;


public:
	FOnCombined OnCombined;
	FOnSplit OnSplit;
	
	UPROPERTY(Replicated)
	bool bCanRecombine;
	bool bIsRecombining;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<ALab_BlockoutStadiumCharacter> PlayerCharacter;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* SBSplitCombine;


protected:
	virtual void BeginPlay() override;

	void OnCanRecombineTimer();

	UFUNCTION()
	void OnCombineHitBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	void CombineWithCollidedChar(ALab_BlockoutStadiumCharacter* CollidedChar);
	

public:
	USACombineSplit();

	void InitComponent(ALab_BlockoutStadiumCharacter* Player, USASizeModifier* SizeModifier);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void Split(const FVector SplitDirection, const float SplitPushForce);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSplit(const FVector SplitDirection, const float SplitPushForce);

	void PlaySound(const float ColVel);
	UFUNCTION(Server, Unreliable)
	void ServerPlaySound(const float ColVel);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastPlaySound(const float ColVel);
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacterController.h"

#include "Lab_BlockoutStadium.h"
#include "Camera/CameraComponent.h"
#include "Kismet/KismetSystemLibrary.h"


APlayerCharacterController::APlayerCharacterController()
{
	CharacterSwapTraceRadius = 50.0f;
	SwapStartOffset = FVector(0, 0, 5);
	SwapOffsetAngle = FRotator(5, 0, 0);
	MaxSwapDistance = 100000.0f;
}


void APlayerCharacterController::BeginPlay()
{
	Super::BeginPlay();

	if (!HasAuthority())
		return;
	
	GameState = Cast<ASportGameState>(GetWorld()->GetGameState());

	if (GameState)
	{
		GameState->OnGameStart.AddDynamic(this, &APlayerCharacterController::OnGameStart);
		GameState->OnGameEnd.AddDynamic(this, &APlayerCharacterController::OnGameEnd);

		GameState->OnRoundReset.AddDynamic(this, &APlayerCharacterController::OnRoundReset);
	}
}


void APlayerCharacterController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if (!GameState)
		GameState = Cast<ASportGameState>(GetWorld()->GetGameState());

	PossessedPlayer = Cast<ALab_BlockoutStadiumCharacter>(InPawn);

	PossessedPlayer->SetLastCharacterController(this);
	PossessedPlayer->GrabThrowComponent->SetGrabBallHighlight(true);
}


void APlayerCharacterController::OnUnPossess()
{
	Super::OnUnPossess();

	if (PossessedPlayer)
	{
		PossessedPlayer->SetLastCharacterController(this);
	}
	
	PossessedPlayer = nullptr;

	// UE_LOG(LogTemp, Warning, TEXT("Player Controller: %s unpossessed"), *GetActorLabel());
}


void APlayerCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("Move Forward / Backward", this, &APlayerCharacterController::MoveForward);
	InputComponent->BindAxis("Move Right / Left", this, &APlayerCharacterController::MoveRight);

	InputComponent->BindAxis("Turn Right / Left Mouse", this, &APlayerCharacterController::MouseX);
	InputComponent->BindAxis("Look Up / Down Mouse", this, &APlayerCharacterController::MouseY);
	
	InputComponent->BindAxis("Turn Right / Left Gamepad", this, &APlayerCharacterController::GamepadX);
	InputComponent->BindAxis("Look Up / Down Gamepad", this, &APlayerCharacterController::GamepadY);

	InputComponent->BindAction("Kick", IE_Pressed, this, &APlayerCharacterController::Kick);
	InputComponent->BindAction("GrabThrow", IE_Pressed, this, &APlayerCharacterController::GrabThrowBall);
	
	InputComponent->BindAction("SwitchCharacter", IE_Pressed, this, &APlayerCharacterController::SwitchCharacter);
	InputComponent->BindAction("SpotSplit", IE_Pressed, this, &APlayerCharacterController::SpotSplit);

	InputComponent->BindAction("Aim", IE_Pressed, this, &APlayerCharacterController::AimStart);
	InputComponent->BindAction("Aim", IE_Released, this, &APlayerCharacterController::AimEnd);

	InputComponent->BindAction("ShootSplit", IE_Pressed, this, &APlayerCharacterController::ShootSplit);

	InputComponent->BindAction("Jump", IE_Pressed, this, &APlayerCharacterController::LaunchStart);
	InputComponent->BindAction("Jump", IE_Released, this, &APlayerCharacterController::LaunchEnd);
}


EPlayerTeam APlayerCharacterController::GetCurrentTeam() const
{
	if (GameState)
	{
		return GameState->GetPlayerTeam(PlayerState);
	}

	return EPlayerTeam::TEAM_NONE;
}


void APlayerCharacterController::SetPlayerLastController(ALab_BlockoutStadiumCharacter* InPlayer)
{
	ServerSetPlayerLastController(InPlayer);
}

void APlayerCharacterController::ServerSetPlayerLastController_Implementation(ALab_BlockoutStadiumCharacter* InPlayer)
{
	InPlayer->SetLastCharacterController(this);
}


void APlayerCharacterController::OnGameStart()
{
	GameState->SetUpPlayer(PlayerState);
	
	// UE_LOG(LogTemp, Warning, TEXT("Player %s has been set up"), *GetActorLabel());
}


void APlayerCharacterController::OnRoundReset()
{
	UnPossess();
	GameState->SetUpPlayer(PlayerState);
}


void APlayerCharacterController::OnGameEnd()
{
	ClientOnGameEnd();
}

void APlayerCharacterController::ClientOnGameEnd_Implementation()
{
	DisableInput(this);
}

void APlayerCharacterController::MoveForward(float Value)
{
	ServerMoveForward(Value);
}

void APlayerCharacterController::ServerMoveForward_Implementation(float Value)
{
	if (PossessedPlayer)
		PossessedPlayer->MoveForward(Value);
}


void APlayerCharacterController::MoveRight(float Value)
{
	ServerMoveRight(Value);
}

void APlayerCharacterController::ServerMoveRight_Implementation(float Value)
{
	if (PossessedPlayer)
		PossessedPlayer->MoveRight(Value);
}


void APlayerCharacterController::MouseX(float Value)
{
	ServerMouseX(Value);
}

void APlayerCharacterController::ServerMouseX_Implementation(float Value)
{
	if (PossessedPlayer && Value != 0.0f)
		PossessedPlayer->MouseX(Value);
}


void APlayerCharacterController::MouseY(float Value)
{
	ServerMouseY(Value);
}

void APlayerCharacterController::ServerMouseY_Implementation(float Value)
{
	if (PossessedPlayer && Value != 0.0f)
		PossessedPlayer->MouseY(Value);
}


void APlayerCharacterController::GamepadX(float Value)
{
	ServerGamepadX(Value);
}

void APlayerCharacterController::ServerGamepadX_Implementation(float Value)
{
	if (PossessedPlayer)
		PossessedPlayer->TurnAtRate(Value);
}


void APlayerCharacterController::GamepadY(float Value)
{
	ServerGamepadY(Value);
}


void APlayerCharacterController::Kick()
{
	ServerKick();
}

void APlayerCharacterController::ServerKick_Implementation()
{
	if (PossessedPlayer)
		PossessedPlayer->Kick();
}


void APlayerCharacterController::ServerGamepadY_Implementation(float Value)
{
	if (PossessedPlayer)
		PossessedPlayer->LookUpAtRate(Value);
}


void APlayerCharacterController::GrabThrowBall()
{
	ServerGrabThrowBall();
}

void APlayerCharacterController::ServerGrabThrowBall_Implementation()
{
	if (PossessedPlayer)
		PossessedPlayer->GrabAndThrow();
}


void APlayerCharacterController::SwitchCharacter()
{
	ServerSwitchCharacter();
}

void APlayerCharacterController::ServerSwitchCharacter_Implementation()
{
	if (!PossessedPlayer)
		return;

	if (PossessedPlayer->CurrentState == EPlayerState::STATE_AIMING || PossessedPlayer->CurrentState == EPlayerState::STATE_CHARGING)
		return;

	ALab_BlockoutStadiumCharacter* CharToSwap = GetFacingCharacter();

	if (CharToSwap && CharToSwap->LastCharacterController == this)
	{
		PrePossessTasks();
		Possess(CharToSwap);
	}
}


void APlayerCharacterController::PrePossessTasks()
{
	ServerPrePossessTasks();
}

void APlayerCharacterController::ServerPrePossessTasks_Implementation()
{
	MulticastPrePossessTasks();
}

void APlayerCharacterController::MulticastPrePossessTasks_Implementation()
{
	if (PossessedPlayer)
	{
		PossessedPlayer->GrabThrowComponent->SetGrabBallHighlight(false);
	}
}


ALab_BlockoutStadiumCharacter* APlayerCharacterController::GetFacingCharacter() const
{
	const FVector Start = PossessedPlayer->GetFollowCamera()->GetComponentLocation() + SwapStartOffset;

	const FRotator CameraAngle = PossessedPlayer->GetFollowCamera()->GetForwardVector().Rotation();
	const FVector LookDir = SwapOffsetAngle.RotateVector(FVector::XAxisVector);
	
	const FVector End = Start + (CameraAngle.RotateVector(LookDir) * MaxSwapDistance);
	
	TArray<FHitResult> OutHits;

	for (auto& Hit : OutHits)
	{
		Hit = FHitResult(ForceInit);
	}

	TArray<AActor*> ActorsToIgnore;
	
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;

	if (GetCurrentTeam() == EPlayerTeam::TEAM1)
	{
		ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_TEAM1));

		// UE_LOG(LogTemp, Warning, TEXT("Player Controller - tracing for team: %s"), *UEnum::GetValueAsString(EPlayerTeam::TEAM1));
	}
	else if (GetCurrentTeam() == EPlayerTeam::TEAM2)
	{
		ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_TEAM2));
		// UE_LOG(LogTemp, Warning, TEXT("Player Controller - tracing for team: %s"), *UEnum::GetValueAsString(EPlayerTeam::TEAM2));
	}
	else
	{
		ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_Pawn));
		// UE_LOG(LogTemp, Warning, TEXT("Player Controller - tracing for team: %s"), *UEnum::GetValueAsString(EPlayerTeam::TEAM_NONE));
	}
	
	if (TraceForFacingCharacter(GetWorld(), ActorsToIgnore, Start, End, OutHits, ObjectTypes))
	{
		for (auto Hit : OutHits)
		{
			if (Hit.GetActor() != this)
			{
				ALab_BlockoutStadiumCharacter* HitCharacter = Cast<ALab_BlockoutStadiumCharacter>(Hit.GetActor());
				if (HitCharacter && HitCharacter != PossessedPlayer)
					return HitCharacter;
			}
		}
	}

	return nullptr;
}


bool APlayerCharacterController::TraceForFacingCharacter(const UWorld* World, const TArray<AActor*>& ActorsToIgnore,
	const FVector& Start, const FVector& End, TArray<FHitResult>& OutHits,
	const TArray<TEnumAsByte<EObjectTypeQuery>>& ObjectTypes) const
{
	if (!World)
		return false;

	for (auto& Hit : OutHits)
	{
		Hit = FHitResult(ForceInit);
	}

	UKismetSystemLibrary::SphereTraceMultiForObjects(World, Start, End, CharacterSwapTraceRadius, ObjectTypes,
		false, ActorsToIgnore, EDrawDebugTrace::None, OutHits, true);

	return (OutHits.Num() > 0);
}


void APlayerCharacterController::SpotSplit()
{
	ServerSpotSplit();
}

void APlayerCharacterController::ServerSpotSplit_Implementation()
{
	if (PossessedPlayer)
		PossessedPlayer->SpotSplit();
}


void APlayerCharacterController::AimStart()
{
	ServerAimStart();
}

void APlayerCharacterController::ServerAimStart_Implementation()
{
	if (PossessedPlayer)
		PossessedPlayer->AimStart();
}


void APlayerCharacterController::AimEnd()
{
	ServerAimEnd();
}

void APlayerCharacterController::ServerAimEnd_Implementation()
{
	if (PossessedPlayer)
		PossessedPlayer->AimEnd();
}


void APlayerCharacterController::ShootSplit()
{
	ServerShootSplit();
}

void APlayerCharacterController::ServerShootSplit_Implementation()
{
	if (PossessedPlayer)
		PossessedPlayer->ShootSplit();
}


void APlayerCharacterController::LaunchStart()
{
	ServerLaunchStart();
}

void APlayerCharacterController::ServerLaunchStart_Implementation()
{
	if (PossessedPlayer)
		PossessedPlayer->LaunchStart();
}


void APlayerCharacterController::LaunchEnd()
{
	ServerLaunchEnd();
}

void APlayerCharacterController::ServerLaunchEnd_Implementation()
{
	if (PossessedPlayer)
		PossessedPlayer->LaunchEnd();
}
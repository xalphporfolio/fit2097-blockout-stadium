// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "WreckingBall.h"
#include "GameFramework/Actor.h"
#include "Lever.generated.h"


UCLASS()
class LAB_BLOCKOUTSTADIUM_API ALever : public AActor
{
	GENERATED_BODY()
	
public:	
	ALever();

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Arm;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Base;

	UPROPERTY(EditAnywhere)
	UPhysicsConstraintComponent* PhysicsConstraint;

	UPROPERTY(EditAnywhere)
	AWreckingBall* WreckingBall;

	
protected:
	virtual void BeginPlay() override;

	
public:	
	virtual void Tick(float DeltaTime) override;

};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SAGrabThrow.h"
#include "Lab_BlockoutStadiumCharacter.h"
#include "NiagaraFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"


USAGrabThrow::USAGrabThrow()
{
	BallHoldOffsets = {100.0f, 120.0f, 150.0f};
	ThrowForce = 500.0f;
}


void USAGrabThrow::InitComponent(ALab_BlockoutStadiumCharacter* Player, USASizeModifier* SizeModifier)
{
	PlayerRef = Player;
	SizeModifierComponent = SizeModifier;

	if (!PlayerRef)
		UE_LOG(LogTemp, Warning, TEXT("GrabThrow Component - No PlayerRef received"));

	if (!SizeModifierComponent)
		UE_LOG(LogTemp, Warning, TEXT("GrabThrow Component - No SizeModifierComponent received"));
}


void USAGrabThrow::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USAGrabThrow, HoveredBall);
	DOREPLIFETIME(USAGrabThrow, GrabbedBall);
}


bool USAGrabThrow::IsGrabbing() const
{
	return GrabbedBall != nullptr;
}


void USAGrabThrow::BeginPlay()
{
	Super::BeginPlay();

	OnComponentBeginOverlap.AddDynamic(this, &USAGrabThrow::OnGrabHitBoxOverlapBegin);
	OnComponentEndOverlap.AddDynamic(this, &USAGrabThrow::OnGrabHitBoxOverlapEnd);
}


void USAGrabThrow::OnComponentDestroyed(bool bDestroyingHierarchy)
{
	Super::OnComponentDestroyed(bDestroyingHierarchy);

	if (GrabbedBall)
	{
		GrabbedBall->ReleaseBall(FVector::UpVector * 300);
		ReleaseBall();	
	}
}


void USAGrabThrow::OnGrabHitBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                            UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (PlayerRef && OtherActor && OtherActor != PlayerRef)
	{
		if (PlayerRef->HasAuthority())
		{
			if (Cast<ABall>(OtherActor))
			{
				if (Cast<ABall>(OtherActor)->bCanBeStolen && !GrabbedBall)
				{
					HoveredBall = Cast<ABall>(OtherActor);

					SetGrabBallHighlight(true);
					// UE_LOG(LogTemp, Warning, TEXT("Ball hovered | Hovered Object: %s"), *HoveredBall->GetActorLabel());
				}
			}
		}
	}
}


void USAGrabThrow::OnGrabHitBoxOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (PlayerRef && OtherActor && OtherActor != PlayerRef)
	{
		if (Cast<ABall>(OtherActor) && (Cast<ABall>(OtherActor) == HoveredBall))
		{
			SetGrabBallHighlight(false);

			if (PlayerRef->HasAuthority())
			{
				HoveredBall = nullptr;

				// UE_LOG(LogTemp, Warning, TEXT("Ball not hovered | Hovered Object: %s"), (HoveredBall ? TEXT("True | %s"), *HoveredBall->GetActorLabel() : TEXT("False")));
			}
		}
	}
}


void USAGrabThrow::OnBallKnockedOff()
{
	ReleaseBall();

	OnKnocked.Broadcast();
}

void USAGrabThrow::OnBallGrabbed()
{
	ReleaseBall();

	OnKnocked.Broadcast();
}


void USAGrabThrow::OnRep_HoveredBall()
{
	if (HoveredBall)
	{
		SetGrabBallHighlight(true);	
	}
}

void USAGrabThrow::OnRep_GrabbedBall()
{
	if (GrabbedBall)
	{
		SetGrabBallHighlight(false);	
	}
}


void USAGrabThrow::GrabAndThrow()
{
	ServerGrabAndThrow();
}

void USAGrabThrow::ServerGrabAndThrow_Implementation()
{
	// grab the ball if no ball and ball in grabbing range
	if (!GrabbedBall && HoveredBall)
	{
		GrabBall();
		return;
	}
	
	// throw the ball if grabbed
	if (GrabbedBall != nullptr)
	{	
		ThrowBall();
		return;
	}

	// UE_LOG(LogTemp, Warning, TEXT("Grab and Throw - nothing executed"));
}


void USAGrabThrow::GrabBall()
{
	ServerGrabBall();
}

void USAGrabThrow::ServerGrabBall_Implementation()
{
	if (PlayerRef && !GrabbedBall && HoveredBall)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Attempting to grab ball"));
		
		GrabbedBall = HoveredBall;
		HoveredBall = nullptr;

		SetGrabBallHighlight(false);
		
		if (SizeModifierComponent->GetCurrentSize() == EActorSize::SIZE_LARGE)
		{
			GrabbedBall->GrabBall(PlayerRef, false);
		}
		else
		{
			GrabbedBall->GrabBall(PlayerRef);	
		}
		
		const FVector GrabPos = FVector(0, 0, BallHoldOffsets[static_cast<int>(SizeModifierComponent->GetCurrentSize())]);
		
		GrabbedBall->SetActorRelativeLocation(GrabPos);
		
		GrabbedBall->OnBallKnockedOff.AddDynamic(this, &USAGrabThrow::OnBallKnockedOff);
		GrabbedBall->OnBallGrabbed.AddDynamic(this, &USAGrabThrow::OnBallGrabbed);

		PlayerRef->SwitchState(EPlayerState::STATE_GRABBING);

		MulticastGrabBall();
	}
}

void USAGrabThrow::MulticastGrabBall_Implementation()
{
	OnGrabbed.Broadcast();
}


void USAGrabThrow::ThrowBall()
{
	ServerThrowBall();
}

void USAGrabThrow::ServerThrowBall_Implementation()
{
	if (PlayerRef && GrabbedBall != nullptr)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Attempting to throw ball"));

		// removing before ReleaseBall Func to avoid triggering delegates 
		GrabbedBall->OnBallKnockedOff.RemoveDynamic(this, &USAGrabThrow::OnBallKnockedOff);
		GrabbedBall->OnBallGrabbed.RemoveDynamic(this, &USAGrabThrow::OnBallGrabbed);

		const FVector ThrowDir = PlayerRef->GetFollowCamera()->GetForwardVector();
		GrabbedBall->ReleaseBall(ThrowDir * ThrowForce);

		MulticastThrowBall(GrabbedBall->GetActorLocation(), PlayerRef->GetFollowCamera()->GetComponentRotation());

		ReleaseBall();
	}
}

void USAGrabThrow::MulticastThrowBall_Implementation(const FVector ParticleSpawnPos, const FRotator ParticleSpawnRot)
{
	if (NSThrow)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), NSThrow, ParticleSpawnPos, ParticleSpawnRot);
	}

	if (PlayerRef && PlayerRef->IsLocallyControlled())
	{
		PlayThrowSound();
	}

	OnThrown.Broadcast();
}


void USAGrabThrow::ReleaseBall()
{
	GrabbedBall->OnBallKnockedOff.RemoveDynamic(this, &USAGrabThrow::OnBallKnockedOff);
	GrabbedBall->OnBallGrabbed.RemoveDynamic(this, &USAGrabThrow::OnBallGrabbed);

	SetGrabBallHighlight(false);
	
	GrabbedBall = nullptr;
	PlayerRef->SwitchState(EPlayerState::STATE_NORMAL);
}


void USAGrabThrow::SetGrabBallHighlight(bool bIsHighlighted)
{
	if (PlayerRef && PlayerRef->HasNetOwner())
		ServerSetGrabBallHighlight(bIsHighlighted);
}

void USAGrabThrow::ServerSetGrabBallHighlight_Implementation(bool bIsHighlighted)
{
	ClientSetGrabBallHighlight(bIsHighlighted);
}

void USAGrabThrow::ClientSetGrabBallHighlight_Implementation(bool bIsHighlighted)
{
	if (PlayerRef && PlayerRef->IsLocallyControlled())
	{
		if (GrabbedBall)
		{
			GrabbedBall->HighlightBall(bIsHighlighted);
			// UE_LOG(LogTemp, Warning, TEXT("Grabbed Ball Highlight"));
			return;
		}

		if (HoveredBall)
		{
			HoveredBall->HighlightBall(bIsHighlighted);
			// UE_LOG(LogTemp, Warning, TEXT("Hovered Ball Highlight"));
		}

		if (bIsHighlighted)
			OnHovered.Broadcast();
	}
}


void USAGrabThrow::PlayThrowSound()
{
	if (PlayerRef && PlayerRef->HasNetOwner())
		ServerPlayThrowSound();
}

void USAGrabThrow::ServerPlayThrowSound_Implementation()
{
	MulticastPlayThrowSound();
}

void USAGrabThrow::MulticastPlayThrowSound_Implementation()
{
	if (SBThrow)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SBThrow, GetComponentLocation());
	}
}
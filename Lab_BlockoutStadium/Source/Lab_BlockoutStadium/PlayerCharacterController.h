// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Lab_BlockoutStadiumCharacter.h"
#include "GameFramework/PlayerController.h"
#include "SportGameState.h"
#include "Net/UnrealNetwork.h"
#include "PlayerCharacterController.generated.h"


UCLASS()
class LAB_BLOCKOUTSTADIUM_API APlayerCharacterController : public APlayerController
{
	GENERATED_BODY()

	
protected:
	ASportGameState* GameState;

	
public:
	APlayerCharacterController();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character Swapping")
	float CharacterSwapTraceRadius;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character Swapping")
	FVector SwapStartOffset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character Swapping")
	FRotator SwapOffsetAngle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character Swapping")
	float MaxSwapDistance;

	ALab_BlockoutStadiumCharacter* PossessedPlayer;
	
	
protected:
	virtual void BeginPlay() override;

	ALab_BlockoutStadiumCharacter* GetFacingCharacter() const;
	bool TraceForFacingCharacter(const UWorld* World, const TArray<AActor*>& ActorsToIgnore, const FVector& Start,
		const FVector& End, TArray<FHitResult>& OutHits, const TArray<TEnumAsByte<EObjectTypeQuery>>& ObjectTypes) const;

	
public:
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;
	
	virtual void SetupInputComponent() override;

	EPlayerTeam GetCurrentTeam() const;

	void SetPlayerLastController(ALab_BlockoutStadiumCharacter* InPlayer);
	UFUNCTION(Server, Reliable)
	void ServerSetPlayerLastController(ALab_BlockoutStadiumCharacter* InPlayer);

	UFUNCTION()
	void OnGameStart();

	UFUNCTION()
	void OnRoundReset();
	
	UFUNCTION()
	void OnGameEnd();
	UFUNCTION(Client, Reliable)
	void ClientOnGameEnd();
	
	void MoveForward(float Value);
	UFUNCTION(Server, Reliable)
	void ServerMoveForward(float Value);
	
	void MoveRight(float Value);
	UFUNCTION(Server, Reliable)
	void ServerMoveRight(float Value);

	void MouseX(float Value);
	UFUNCTION(Server, Reliable)
	void ServerMouseX(float Value);
	
	void MouseY(float Value);
	UFUNCTION(Server, Reliable)
	void ServerMouseY(float Value);
	
	void GamepadX(float Value);
	UFUNCTION(Server, Reliable)
	void ServerGamepadX(float Value);
	
	void GamepadY(float Value);
	UFUNCTION(Server, Reliable)
	void ServerGamepadY(float Value);

	void Kick();
	UFUNCTION(Server, Reliable)
	void ServerKick();
	
	void GrabThrowBall();
	UFUNCTION(Server, Reliable)
	void ServerGrabThrowBall();
	
	void SwitchCharacter();
	UFUNCTION(Server, Reliable)
	void ServerSwitchCharacter();

	void PrePossessTasks();
	UFUNCTION(Server, Reliable)
	void ServerPrePossessTasks();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastPrePossessTasks();
	
	void SpotSplit();
	UFUNCTION(Server, Reliable)
	void ServerSpotSplit();

	void AimStart();
	UFUNCTION(Server, Reliable)
	void ServerAimStart();
		
	void AimEnd();
	UFUNCTION(Server, Reliable)
	void ServerAimEnd();
	
	void ShootSplit();
	UFUNCTION(Server, Reliable)
	void ServerShootSplit();

	void LaunchStart();
	UFUNCTION(Server, Reliable)
	void ServerLaunchStart();
	
	void LaunchEnd();
	UFUNCTION(Server, Reliable)
	void ServerLaunchEnd();
};



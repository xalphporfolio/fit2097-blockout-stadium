// Fill out your copyright notice in the Description page of Project Settings.


#include "SportPlayerState.h"
#include "GameFramework/GameStateBase.h"


ASportPlayerState::ASportPlayerState()
{
	bReplicates = true;
	
	OnFieldName = "";

	Kicks = 0;
	BallGrabs = 0;
	BallThrows = 0;
	GoalsScored = 0;
	TimesSplit = 0;
	TimesShot = 0;
	TimesLaunched = 0;
	TimesKnockedBallUp = 0;
	TimesKnockedBallForward = 0;
}


void ASportPlayerState::ClientInitialize(AController* C)
{
	Super::ClientInitialize(C);

	if (HasAuthority())
	{
		GetWorld()->GetGameState()->AddPlayerState(this);
	}
}


void ASportPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASportPlayerState, OnFieldName);

	DOREPLIFETIME(ASportPlayerState, Kicks);
	DOREPLIFETIME(ASportPlayerState, BallGrabs);
	DOREPLIFETIME(ASportPlayerState, BallThrows);
	DOREPLIFETIME(ASportPlayerState, GoalsScored);
	DOREPLIFETIME(ASportPlayerState, TimesSplit);
	DOREPLIFETIME(ASportPlayerState, TimesShot);
	DOREPLIFETIME(ASportPlayerState, TimesLaunched);
	DOREPLIFETIME(ASportPlayerState, TimesKnockedBallUp);
	DOREPLIFETIME(ASportPlayerState, TimesKnockedBallForward);
}


void ASportPlayerState::SetFieldName(const FString& NewName)
{
	ServerSetFieldName(NewName);
}

void ASportPlayerState::ServerSetFieldName_Implementation(const FString& NewName)
{
	OnFieldName = NewName;	
}


void ASportPlayerState::AddKicks(const int Amount)
{
	ServerAddKicks(Amount);
}

bool ASportPlayerState::ServerAddKicks_Validate(const int Amount)
{
	if (Amount > 1 || Amount < 0)
		return false;

	return true;
}

void ASportPlayerState::ServerAddKicks_Implementation(const int Amount)
{
	Kicks += Amount;
}


void ASportPlayerState::AddBallGrabs(const int Amount)
{
	ServerAddBallGrabs(Amount);
}

bool ASportPlayerState::ServerAddBallGrabs_Validate(const int Amount)
{
	if (Amount > 1 || Amount < 0)
		return false;

	return true;
}

void ASportPlayerState::ServerAddBallGrabs_Implementation(const int Amount)
{
	BallGrabs += Amount;
}


void ASportPlayerState::AddBallThrows(const int Amount)
{
	ServerAddBallThrows(Amount);
}

bool ASportPlayerState::ServerAddBallThrows_Validate(const int Amount)
{
	if (Amount > 1 || Amount < 0)
		return false;

	return true;
}

void ASportPlayerState::ServerAddBallThrows_Implementation(const int Amount)
{
	BallThrows += Amount;
}


void ASportPlayerState::AddGoals(const int Amount)
{
	ServerAddGoals(Amount);
}

bool ASportPlayerState::ServerAddGoals_Validate(const int Amount)
{
	if (Amount > 1 || Amount < 0)
		return false;

	return true;
}

void ASportPlayerState::ServerAddGoals_Implementation(const int Amount)
{
	GoalsScored += Amount;
}


void ASportPlayerState::AddTimesSplit(const int Amount)
{
	ServerTimeSplit(Amount);
}

bool ASportPlayerState::ServerTimeSplit_Validate(const int Amount)
{
	if (Amount > 1 || Amount < 0)
		return false;

	return true;
}

void ASportPlayerState::ServerTimeSplit_Implementation(const int Amount)
{
	TimesSplit += Amount;
}


void ASportPlayerState::AddTimesShot(const int Amount)
{
	ServerAddTimesShot(Amount);
}

bool ASportPlayerState::ServerAddTimesShot_Validate(const int Amount)
{
	if (Amount > 1 || Amount < 0)
		return false;

	return true;
}

void ASportPlayerState::ServerAddTimesShot_Implementation(const int Amount)
{
	TimesShot += Amount;
}


void ASportPlayerState::AddTimesLaunched(const int Amount)
{
	ServerAddTimesLaunched(Amount);
}

bool ASportPlayerState::ServerAddTimesLaunched_Validate(const int Amount)
{
	if (Amount > 1 || Amount < 0)
		return false;

	return true;
}

void ASportPlayerState::ServerAddTimesLaunched_Implementation(const int Amount)
{
	TimesLaunched += Amount;
}


void ASportPlayerState::AddTimesKnockedBallUp(const int Amount)
{
	ServerAddTimesKnockedBallUp(Amount);
}

bool ASportPlayerState::ServerAddTimesKnockedBallUp_Validate(const int Amount)
{
	if (Amount > 1 || Amount < 0)
		return false;

	return true;
}

void ASportPlayerState::ServerAddTimesKnockedBallUp_Implementation(const int Amount)
{
	TimesKnockedBallUp += Amount;
}


void ASportPlayerState::AddTimesKnockedBallForward(const int Amount)
{
	ServerAddTimesKnockedBallForward(Amount);
}

bool ASportPlayerState::ServerAddTimesKnockedBallForward_Validate(const int Amount)
{
	if (Amount > 1 || Amount < 0)
		return false;

	return true;
}

void ASportPlayerState::ServerAddTimesKnockedBallForward_Implementation(const int Amount)
{
	TimesKnockedBallForward += Amount;
}

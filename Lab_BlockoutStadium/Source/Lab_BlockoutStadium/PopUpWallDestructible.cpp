// Fill out your copyright notice in the Description page of Project Settings.


#include "PopUpWallDestructible.h"

#include "Lab_BlockoutStadium.h"
#include "GeometryCollection/GeometryCollectionComponent.h"


APopUpWallDestructible::APopUpWallDestructible()
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	WallBase = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallBase"));
	WallBase->SetupAttachment(RootComponent);

	WallOutline = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallOutline"));
	WallOutline->SetupAttachment(RootComponent);
	
	IsFilled = false;
}


void APopUpWallDestructible::BeginPlay()
{
	Super::BeginPlay();
	
	// grab reference to GC and set up collisions

	// Note: 4/10/2022
	// For some reason, you cannot store a direct reference to a UGeometryCollectionComponent class
	// meaning it is difficult to access its methods or the component itself.
	// The  solution I have found is to store a reference to a Geometry Collection as a AActor and then
	// cast it to a UGeometryCollectionComponent when I need to access its methods.
	// It is not the most efficient method, but is the best I have due to this issue. This is likely a
	// problem due to how new Chaos at this point in development
	
	TArray<UActorComponent*> AllComponents = GetInstanceComponents();
	
	if (AllComponents.Num() > 0)
		UE_LOG(LogTemp, Warning, TEXT("Found Components"));
	
	for (UActorComponent* Component : GetComponents())
	{
		if (Component->ComponentHasTag("DestructibleWall"))
		{
			DestructibleWall = Component;
			UE_LOG(LogTemp, Warning, TEXT("Found DestructibleWall"));
		}
	}
	
	if (!DestructibleWall)
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not find destructible wall"));
		return;
	}
	
	// Cast<UGeometryCollectionComponent>(DestructibleWall)->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	// // Cast<UGeometryCollectionComponent>(DestructibleWall)->SetDynamicState(Chaos::EObjectStateType::Static);
	//
	// // SpawnStateSettingField(3);
	//
	// Cast<UGeometryCollectionComponent>(DestructibleWall)->SetRenderCustomDepth(false);
	
	// set up switch
	if (ConnectedButton)
	{
		ConnectedButton->OnButtonPressed.AddDynamic(this, &APopUpWallDestructible::OnButtonPressed);
	}
	
	// get and assign materials
	// if (MaterialClassOutline)
	// {
	// 	MaterialOutlineInstance = UMaterialInstanceDynamic::Create(MaterialClassOutline, this);
	// 	
	// 	Cast<UGeometryCollectionComponent>(DestructibleWall)->SetMaterial(0, MaterialOutlineInstance);
	// 	Cast<UGeometryCollectionComponent>(DestructibleWall)->SetMaterial(1, MaterialOutlineInstance);
	// }
	//
	// if (MaterialClassFilled)
	// {
	// 	MaterialFilledInstance = UMaterialInstanceDynamic::Create(MaterialClassFilled, this);
	// }
	// if (MaterialClassFilledInside)
	// {
	// 	MaterialFilledInsideInstance = UMaterialInstanceDynamic::Create(MaterialClassFilledInside, this);
	// }
}

void APopUpWallDestructible::SpawnStateSettingField(int ChaosState)
{
	const FRotator SpawnRotation = this->GetActorRotation();
	const FVector SpawnLocation = this->GetActorLocation();
	
	FActorSpawnParameters ActorSpawnParameters;
	ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			
	ASetStaticDynamicField* SpawnedField = GetWorld()->SpawnActor<ASetStaticDynamicField>(SetStaticDynamicFieldClass, SpawnLocation, SpawnRotation, ActorSpawnParameters);

	if (SpawnedField)
	{
		SpawnedField->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);

		SpawnedField->SetActorRelativeLocation(FVector(0.0f, 230.0f, 110.0f));
	}
	
	SpawnedField->CreateField(ChaosState, FVector(1.0f, 8.5f, 5.5f));
}


void APopUpWallDestructible::OnButtonPressed()
{
	UE_LOG(LogTemp, Warning, TEXT("Wall recieved button press"));
	
	// if (!DestructibleWall)
	// 	return;

	if (!GeoTest)
		return;
	
	if (IsFilled)
		return;

	IsFilled = true;
	

	WallOutline->DestroyComponent();

	//Cast<UGeometryCollectionComponent>(DestructibleWall)->OnChaosBreakEvent
	
	// // Cast<UGeometryCollectionComponent>(DestructibleWall)->SetRestCollection(GeoTest);
	// Cast<UGeometryCollectionComponent>(DestructibleWall)->RestCollection = GeoTest;
	//
	// Cast<UGeometryCollectionComponent>(DestructibleWall)->EditRestCollection(GeometryCollection::EEditUpdate::RestPhysicsDynamic);
	//
	// // Cast<UGeometryCollectionComponent>(DestructibleWall)->SetRenderCustomDepth(true);
	//
	// Cast<UGeometryCollectionComponent>(DestructibleWall)->OnChaosBreakEvent.AddDynamic()

	// Cast<UGeometryCollectionComponent>(DestructibleWall)->SetDynamicState(Chaos::EObjectStateType::Dynamic);
	//Cast<UGeometryCollectionComponent>(DestructibleWall)->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	// SpawnStateSettingField(4);
	
	/*Cast<UGeometryCollectionComponent>(DestructibleWall)->SetRenderCustomDepth(true);

	if (MaterialFilledInstance)
	{
		Cast<UGeometryCollectionComponent>(DestructibleWall)->SetMaterial(0, MaterialFilledInstance);
		Cast<UGeometryCollectionComponent>(DestructibleWall)->SetMaterial(1, MaterialFilledInsideInstance);	
	}*/
}

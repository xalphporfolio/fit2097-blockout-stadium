// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "NiagaraSystem.h"
#include "SportGameState.h"
#include "Net/UnrealNetwork.h"
#include "NiagaraDataInterfaceExport.h"
#include "SAAcidPatch.h"
#include "SACombineSplit.h"
#include "SAGrabThrow.h"
#include "SAJumpLaunch.h"
#include "SAKick.h"
#include "SASizeModifier.h"
#include "Components/BoxComponent.h"
#include "Lab_BlockoutStadiumCharacter.generated.h"

class APlayerCharacterController;
class ABall;


UENUM(BlueprintType)
enum class EPlayerState:uint8
{
	STATE_NORMAL UMETA(DisplayName = "Normal"),
	STATE_AIMING UMETA(DisplayName = "Aiming"),
	STATE_CHARGING UMETA(DisplayName = "Charging"),
	STATE_FLYING UMETA(DisplayName = "Flying"),
	STATE_GRABBING UMETA(DisplayName = "Grabbing"),
};


UCLASS(config=Game)
class ALab_BlockoutStadiumCharacter : public ACharacter, public INiagaraParticleCallbackHandler
{
	GENERATED_BODY()

	
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	// jiggle shader
	FVector PreviousVelocity;
	FVector CurrentMaterialVelocity;
	
	bool bVerticalBouncing;
	FTimerHandle VerticalBounceTimerHandle;

	bool bHorizontalBouncing;
	FVector LastHorizontalDir;
	FTimerHandle HorizontalBounceTimerHandle;


protected:
	UPROPERTY()
	ASportGameState* GameState;
	
	
public:
	ALab_BlockoutStadiumCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Input)
	float TurnRateGamepad;

	// ability components 
	UPROPERTY(EditAnywhere)
	USASizeModifier* SizeModifierComponent;
	UPROPERTY(EditAnywhere)
	USACombineSplit* CombineSplitComponent;
	UPROPERTY(EditAnywhere)
	USAJumpLaunch* JumpLaunchComponent;
	UPROPERTY(EditAnywhere)
	USAKick* KickComponent;
	UPROPERTY(EditAnywhere)
	USAGrabThrow* GrabThrowComponent;
	UPROPERTY(EditAnywhere)
	USAAcidPatch* AcidPatchComponent;
	
	// teams
	UPROPERTY(Replicated)
	APlayerCharacterController* LastCharacterController;
	UPROPERTY(Replicated)
	APlayerState* LastPlayerState;

	UPROPERTY(EditAnywhere)
	UMaterialInterface* MaterialClass;
	UPROPERTY(EditAnywhere)
	UMaterialInstanceDynamic* MaterialInstance;
	
	// state properties
	UPROPERTY(Replicated)
	EPlayerState CurrentState;
	UPROPERTY(Replicated)
	EPlayerState PreviousState;

	// jiggle shader
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Jiggle Shader")
	float MaxVerticalBounce;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Jiggle Shader")
	float MaxHorizontalBounce;

	// speed multiplier
	float SpeedMultiplier;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Speed Multiplier")
	float FlyingSpeedMultiplier;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, EditFixedSize, Category = "Size", meta = (EditFixedOrder))
	TArray<float> MoveSpeeds;

	// collision particles
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Collision Particles")
	UNiagaraSystem* NS_SlimePieces;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Collision Particles")
	float SlimePiecesVelocityThreshold;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, EditFixedSize, Category = "Collision Particles", meta = (EditFixedOrder))
	TArray<int> SlimePiecesAmount;
	
	// bouncing
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, EditFixedSize, Category = "Bouncing", meta = (EditFixedOrder))
	TArray<float> BounceForceMultiplier;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Bouncing")
	float MinPlayerBounce;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Bouncing")
	float MaxPlayerBounce;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Bouncing")
	float MinBallUpBounce;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Bouncing")
	float MaxBallUpBounce;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Bouncing")
	float MinBallForwardBounce;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Bouncing")
	float MaxBallForwardBounce;
	
	// grabbing
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|Grab")
	float GrabSpeedMultiplier;

	// shooting
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|CameraDefaults")
	float DefaultCameraDistance;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|CameraDefaults")
	FVector DefaultCameraOffset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|Shoot")
	float AimingCameraDistance;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|Shoot")
	FVector AimingCameraOffset;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|Shoot")
	float ShootSplitForce;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|Shoot")
	float AimSpeedMultiplier;

	// jumping and launching
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|Launch")
	float ChargingCameraDistance;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|Launch")
	FVector ChargingCameraOffset;
	
	// acid patches
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|AcidPatches")
	float AcidParticleVelocityThreshold;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|AcidPatches")
	float AcidPatchVelocityThreshold;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|AcidPatches")
	float AcidParticleMinSize;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities|AcidPatches")
	float AcidParticleMaxSize;
	
	// audio
	bool bIsMoveSoundsPlaying;

	UPROPERTY()
	UAudioComponent* MoveAudioComponent;
	
	UPROPERTY(EditDefaultsOnly, Category = "Audio|SoundEffects")
	USoundBase* SBMoveAudio;;
	UPROPERTY(EditDefaultsOnly, Category = "Audio|SoundEffects")
	USoundBase* SBCollision;

	
protected:
	virtual void BeginPlay() override;
	virtual void UnPossessed() override;

	UFUNCTION()
	void OnRoundReset();
	
	UFUNCTION()
	void OnDestroy(AActor* DestroyedActor);

	UFUNCTION()
	void OnCapsuleHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		FVector NormalImpulse, const FHitResult& Hit);

	void HandlePlayerOnPlayerBounce(const ALab_BlockoutStadiumCharacter* HitPlayer, const FHitResult& Hit);
	void HandlePlayerBallHit(ABall* HitBall, const FHitResult& Hit);
	
	UFUNCTION()
	void OnSizeChanged(const EActorSize NewSize);

	UFUNCTION()
	void OnCombined();
	UFUNCTION()
	void OnSplit();
	
	UFUNCTION()
	void OnCharacterJump();
	UFUNCTION()
	void OnCharacterLaunched();

	UFUNCTION()
	void OnKickHit();
	
	UFUNCTION()
	void OnGrabbed();
	UFUNCTION()
	void OnThrown();

	void UpdateBodyJiggle(float DeltaTime);
	
	float GetBounceSinDamp(const float CurrentTime) const;	
	void OnVerticalBounceTimer();
	void OnHorizontalBounceTimer();

	
public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	virtual void Tick(float DeltaSeconds) override;

	virtual void ReceiveParticleData_Implementation(const TArray<FBasicParticleData>& Data, UNiagaraSystem* NiagaraSystem,
		const FVector& SimulationPositionOffset) override;

	// (these were from the original third-person C++ template)
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	
	void AdjustCamera(float CameraDistance, FVector CameraOffset, bool LockCharRotationToCamera);
	UFUNCTION(Server, Reliable)
	void ServerAdjustCamera(float CameraDistance, FVector CameraOffset, bool LockCharRotationToCamera);
	UFUNCTION(Client, Reliable)
	void ClientAdjustCamera(float CameraDistance, FVector CameraOffset, bool LockCharRotationToCamera);

	void ResetCamera();
	
	void MoveForward(float Value);
	UFUNCTION(Server, Reliable)
	void ServerMoveForward(float Value);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastMoveForward(float Value);

	void MoveRight(float Value);
	UFUNCTION(Server, Reliable)
	void ServerMoveRight(float Value);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastMoveRight(float Value);

	void MouseX(float Value);
	UFUNCTION(Server, Reliable)
	void ServerMouseX(float Value);
	UFUNCTION(Client, Reliable)
	void ClientMouseX(float Value);

	void MouseY(float Value);
	UFUNCTION(Server, Reliable)
	void ServerMouseY(float Value);
	UFUNCTION(Client, Reliable)
	void ClientMouseY(float Value);
	
	void TurnAtRate(float Rate);
	UFUNCTION(Server, Reliable)
	void ServerTurnAtRate(float Rate);
	UFUNCTION(Client, Reliable)
	void MulticastTurnAtRate(float Rate);
	
	void LookUpAtRate(float Rate);
	UFUNCTION(Server, Reliable)
	void ServerLookUpAtRate(float Rate);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastLookUpAtRate(float Rate);

	void SetLastCharacterController(APlayerCharacterController* NewPreviousController);
	UFUNCTION(Server, Reliable)
	void ServerSetLastCharacterController(APlayerCharacterController* NewPreviousController);
	
	EPlayerTeam GetCurrentTeam() const;
	FLinearColor GetCurrentColour() const;

	void SetTeam(EPlayerTeam NewTeam);
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetTeam(EPlayerTeam NewTeam);

	void SetColour(EPlayerTeam CurrentTeam);
	UFUNCTION(Server, Unreliable)
	void ServerSetColour(EPlayerTeam CurrentTeam);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastSetColour(EPlayerTeam CurrentTeam);
	
	void SwitchState(EPlayerState NewState);
	UFUNCTION(Server, Reliable)
	void ServerSwitchState(EPlayerState NewState);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastSwitchState(EPlayerState NewState);
	
	void Kick() const;

	void GrabAndThrow() const;

	void SpotSplit();
	UFUNCTION(Server, Reliable)
	void ServerSpotSplit();

	void AimStart();
	UFUNCTION(Server, Reliable)
	void ServerAimStart();

	void AimEnd();
	UFUNCTION(Server, Reliable)
	void ServerAimEnd();

	void ShootSplit();
	UFUNCTION(Server, Reliable)
	void ServerShootSplit();

	void LaunchStart() const;
	void LaunchEnd() const;

	void SpawnCollisionParticles(FVector ColDir);
	UFUNCTION(Server, Unreliable)
	void ServerSpawnCollisionParticles(FVector ColDir);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastSpawnCollisionParticles(FVector ColDir);

	void PlayOneOffSound(USoundBase* Sound);
	UFUNCTION(Server, Unreliable)
	void ServerPlayOneOffSound(USoundBase* Sound);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastPlayOneOffSound(USoundBase* Sound);

	void PlayCollisionSound(float ColVel);
	UFUNCTION(Server, Unreliable)
	void ServerPlayCollisionSound(float ColVel);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastPlayCollisionSound(float ColVel);
	
	void StartMoveSounds();
	UFUNCTION(Server, Unreliable)
	void ServerStartMoveSounds();
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastStartMoveSounds();
	
	void StopMoveSounds();
	UFUNCTION(Server, Unreliable)
	void ServerStopMoveSounds();
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastStopMoveSounds();
};
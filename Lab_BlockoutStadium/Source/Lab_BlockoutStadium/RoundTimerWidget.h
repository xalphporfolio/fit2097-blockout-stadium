// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "SportGameState.h"
#include "RoundTimerWidget.generated.h"


UCLASS(Abstract)
class LAB_BLOCKOUTSTADIUM_API URoundTimerWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* RoundTimeText;

	ASportGameState* GameState;

	
public:
	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, const float InDeltaTime) override;
};

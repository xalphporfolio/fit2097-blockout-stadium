// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerEndUIWidget.h"
#include "SportPlayerState.h"


void UPlayerEndUIWidget::NativeConstruct()
{
	Super::NativeConstruct();

	// set values from local player state
	if (GetOwningPlayer()->GetPlayerState<ASportPlayerState>())
	{
		const ASportPlayerState* CurrentPlayerState = GetOwningPlayer()->GetPlayerState<ASportPlayerState>();

		FieldNameText->SetText(FText::FromString(CurrentPlayerState->OnFieldName));

		// player stats
		KicksText->SetText(FText::AsNumber(CurrentPlayerState->Kicks));
		BallGrabsText->SetText(FText::AsNumber(CurrentPlayerState->BallGrabs));
		BallThrowsText->SetText(FText::AsNumber(CurrentPlayerState->BallThrows));
		GoalsScoredText->SetText(FText::AsNumber(CurrentPlayerState->GoalsScored));
		TimesSplitText->SetText(FText::AsNumber(CurrentPlayerState->TimesSplit));
		TimeShotText->SetText(FText::AsNumber(CurrentPlayerState->TimesShot));
		TimesLaunchedText->SetText(FText::AsNumber(CurrentPlayerState->TimesLaunched));
		TimesKnockedUpText->SetText(FText::AsNumber(CurrentPlayerState->TimesKnockedBallUp));
		TimesKnockedForwardText->SetText(FText::AsNumber(CurrentPlayerState->TimesKnockedBallForward));
	}
}

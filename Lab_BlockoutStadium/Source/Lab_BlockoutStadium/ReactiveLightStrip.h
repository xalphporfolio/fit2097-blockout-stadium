// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SportGameState.h"
#include "Components/RectLightComponent.h"
#include "GameFramework/Actor.h"
#include "ReactiveLightStrip.generated.h"


UCLASS()
class LAB_BLOCKOUTSTADIUM_API AReactiveLightStrip : public AActor
{
	GENERATED_BODY()
	
public:	
	AReactiveLightStrip();

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* LightBase;

	UPROPERTY(EditDefaultsOnly)
	URectLightComponent* Light;


protected:
	ASportGameState* GameState;

	
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnGameStart();

	UFUNCTION()
	void OnRoundReset();
	
	UFUNCTION()
	void OnTeamOneScored();

	UFUNCTION()
	void OnTeamTwoScored();
};

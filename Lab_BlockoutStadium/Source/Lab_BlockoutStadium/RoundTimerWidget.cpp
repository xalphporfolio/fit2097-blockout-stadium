// Fill out your copyright notice in the Description page of Project Settings.


#include "RoundTimerWidget.h"

#include "Kismet/KismetTextLibrary.h"


void URoundTimerWidget::NativeConstruct()
{
	Super::NativeConstruct();

	GameState = Cast<ASportGameState>(GetWorld()->GetGameState());
}


void URoundTimerWidget::NativeTick(const FGeometry& MyGeometry, const float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (GameState && GameState->bGameStarted)
	{
		const float CurrentTime = GameState->CurrentRoundTimeRemaining;
		
		const int Minutes = CurrentTime / 60;
		const int Seconds = fmodf(CurrentTime, 60);

		const FString TimeString = FString::Printf(TEXT("%02d:%02d"), Minutes, Seconds);

		RoundTimeText->SetText(FText::FromString(TimeString));
	}
}

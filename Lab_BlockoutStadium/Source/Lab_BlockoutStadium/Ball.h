// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SportGameState.h"
#include "Net/UnrealNetwork.h"
#include "Ball.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBallKnockedOff);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBallGrabbed);


UCLASS()
class LAB_BLOCKOUTSTADIUM_API ABall : public AActor
{
	GENERATED_BODY()


protected:
	ASportGameState* GameState;


public:	
	ABall();

	UPROPERTY(BlueprintAssignable)
	FOnBallKnockedOff OnBallKnockedOff;
	UPROPERTY(BlueprintAssignable)
	FOnBallGrabbed OnBallGrabbed;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* BallMesh;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* BallSubstanceMesh;

	UPROPERTY(Replicated)
	ALab_BlockoutStadiumCharacter* LastPlayer;
	
	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* MaterialClassGlass;
	UMaterialInstanceDynamic* MaterialGlassInstance;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* MaterialClassSubstance;
	UMaterialInstanceDynamic* MaterialSubstanceInstance;

	UPROPERTY(EditDefaultsOnly)
	float GroundCheckDistance;

	UPROPERTY(EditDefaultsOnly)
	float StateInterpSpeed;

	UPROPERTY(Replicated)
	bool bIsGrabbed;
	UPROPERTY(Replicated)
	bool bCanBeStolen;
	

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnHitBegin(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	
public:	
	virtual void Tick(float DeltaTime) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	bool IsOnGround();

	void SetLastPlayer(ALab_BlockoutStadiumCharacter* NewLastPlayer);
	UFUNCTION(Server, Reliable)
	void ServerSetLastPlayer(ALab_BlockoutStadiumCharacter* NewLastPlayer);

	void GrabBall(ALab_BlockoutStadiumCharacter* PlayerGrabbing, bool bCanSteal = true);
	UFUNCTION(Server, Reliable)
	void ServerGrabBall(ALab_BlockoutStadiumCharacter* PlayerGrabbing, bool bCanSteal = true);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastGrabBall(ALab_BlockoutStadiumCharacter* PlayerGrabbing, bool bCanSteal = true);
	
	void ReleaseBall(FVector ReleaseForce);
	UFUNCTION(Server, Reliable)
	void ServerReleaseBall(FVector ReleaseForce);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastReleaseBall(FVector ReleaseForce);

	void HighlightBall(bool bIsHighlighted);
	UFUNCTION(Client, Reliable)
	void ClientHighlightBall(bool bIsHighlighted);
	
	UFUNCTION(Server, Unreliable)
	void ServerChangeColour(EPlayerTeam Team);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastChangeColour(EPlayerTeam Team);

	void ChangeState(float BallStateValue);
	UFUNCTION(Server, Unreliable)
	void ServerChangeState(float BallStateValue);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastChangeState(float BallStateValue);
};

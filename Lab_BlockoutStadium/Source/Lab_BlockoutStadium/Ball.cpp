// Fill out your copyright notice in the Description page of Project Settings.


#include "Ball.h"
#include "Lab_BlockoutStadiumCharacter.h"

#include "Kismet/KismetMathLibrary.h"


ABall::ABall()
{
 	
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	NetPriority = 3.0f;
	
	BallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	BallMesh->SetupAttachment(RootComponent);

	BallMesh->SetSimulatePhysics(true);
	BallMesh->SetIsReplicated(true);
	
	BallMesh->SetCollisionObjectType(ECC_PhysicsBody);

	BallSubstanceMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Substance Mesh Component"));
	BallSubstanceMesh->SetupAttachment(BallMesh);

	GroundCheckDistance = 60.0f;

	StateInterpSpeed = 10.0f;

	bIsGrabbed = false;
	bCanBeStolen = true;
}


void ABall::BeginPlay()
{
	Super::BeginPlay();
	
	GameState = Cast<ASportGameState>(GetWorld()->GetGameState());
	
	BallMesh->OnComponentHit.AddDynamic(this, &ABall::OnHitBegin);
	BallMesh->OnComponentBeginOverlap.AddDynamic(this, &ABall::OnOverlapBegin);

	if (MaterialClassGlass)
	{
		MaterialGlassInstance = UMaterialInstanceDynamic::Create(MaterialClassGlass, this);
		BallMesh->SetMaterial(0, MaterialGlassInstance);
	}

	if (MaterialClassSubstance)
	{
		MaterialSubstanceInstance = UMaterialInstanceDynamic::Create(MaterialClassSubstance, this);
		BallSubstanceMesh->SetMaterial(0, MaterialSubstanceInstance);
	}
}


void ABall::OnHitBegin(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                       FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && OtherActor != this)
	{
		if (HasAuthority())
		{
			//UE_LOG(LogTemp, Warning, TEXT("Ball - OnHit Called"));

			if (!bCanBeStolen)
				return;
			
			if (const ALab_BlockoutStadiumCharacter* HitPlayer = Cast<ALab_BlockoutStadiumCharacter>(OtherActor))
			{
				if (bIsGrabbed && HitPlayer == LastPlayer)
					return;
				
				SetLastPlayer(Cast<ALab_BlockoutStadiumCharacter>(OtherActor));
			}
		}
	}
}


void ABall::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != this)
	{
		if (HasAuthority())
		{
			// handle grabbing and stealing while in a player's possession
		
			if (!bIsGrabbed || !bCanBeStolen)
				return;
			
			//UE_LOG(LogTemp, Warning, TEXT("Ball - On Overlapp Begin Called"));
			
			const ALab_BlockoutStadiumCharacter* OverlappedPlayer = Cast<ALab_BlockoutStadiumCharacter>(OtherActor);

			if (OverlappedPlayer)
			{
				if (GameState->GetPlayerTeam(OverlappedPlayer->LastPlayerState) !=
					GameState->GetPlayerTeam(LastPlayer->LastPlayerState))
				{
					if (OverlappedPlayer->CurrentState == EPlayerState::STATE_FLYING)
					{
						ReleaseBall(OverlappedPlayer->GetVelocity() * 0.5f);

						OnBallKnockedOff.Broadcast();

						SetLastPlayer(Cast<ALab_BlockoutStadiumCharacter>(OtherActor));
					}
				}
			}
		}
	}
}


void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!HasAuthority())
		return;

	if (MaterialSubstanceInstance)
	{
		if (IsOnGround())
		{
			ChangeState(UKismetMathLibrary::FInterpTo(MaterialSubstanceInstance->K2_GetScalarParameterValue("BallState"), 0, DeltaTime, StateInterpSpeed));
		}
		else
		{
			ChangeState(UKismetMathLibrary::FInterpTo(MaterialSubstanceInstance->K2_GetScalarParameterValue("BallState"), 1, DeltaTime, StateInterpSpeed));
		}	
	}
}


void ABall::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	DOREPLIFETIME(ABall, LastPlayer);
	DOREPLIFETIME(ABall, bIsGrabbed);
	DOREPLIFETIME(ABall, bCanBeStolen);
}


bool ABall::IsOnGround()
{
	FHitResult OutHit;
	TArray<AActor*> ActorsToIgnore;
	ActorsToIgnore.Add(this);

	FCollisionQueryParams TraceParams(FName(TEXT("Ball Ground Trace")), true, ActorsToIgnore[0]);

	TraceParams.bTraceComplex = true;
	TraceParams.bReturnPhysicalMaterial = false;
	TraceParams.AddIgnoredActors(ActorsToIgnore);

	// debug
	/*const FName TraceTag("Ball Ground Trace");
	GetWorld()->DebugDrawTraceTag = TraceTag;
	TraceParams.TraceTag = TraceTag;*/
	
	// clear hit data
	OutHit = FHitResult(ForceInit);

	FVector Start = GetActorLocation();
	FVector End = Start;
	End.Z -= GroundCheckDistance;

	GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_WorldStatic, TraceParams);

	// UE_LOG(LogTemp, Warning, TEXT("Ball IsOnGround: %s"),  ( (OutHit.GetActor() != nullptr) ? TEXT("true") : TEXT("false") ));

	return (OutHit.GetActor() != nullptr);
}


void ABall::SetLastPlayer(ALab_BlockoutStadiumCharacter* NewLastPlayer)
{
	ServerSetLastPlayer(NewLastPlayer);
}


void ABall::ServerSetLastPlayer_Implementation(ALab_BlockoutStadiumCharacter* NewLastPlayer)
{
	LastPlayer = NewLastPlayer;
	
	//UE_LOG(LogTemp, Warning, TEXT("New Last Player Hit: %s"), *NewLastPlayer->GetActorLabel());

	ServerChangeColour(LastPlayer->GetCurrentTeam());
}


void ABall::GrabBall(ALab_BlockoutStadiumCharacter* PlayerGrabbing, bool bCanSteal)
{
	ServerGrabBall(PlayerGrabbing, bCanSteal);
	// UE_LOG(LogTemp, Warning, TEXT("Ball - Ball Grabbed Base Func"));
}

void ABall::ServerGrabBall_Implementation(ALab_BlockoutStadiumCharacter* PlayerGrabbing, bool bCanSteal)
{
	//UE_LOG(LogTemp, Warning, TEXT("Ball - Ball Grabbed"));
	
	if (bIsGrabbed && PlayerGrabbing == LastPlayer)
		return;
	
	bIsGrabbed = true;
	bCanBeStolen = bCanSteal;
	SetLastPlayer(PlayerGrabbing);

	MulticastGrabBall(PlayerGrabbing, bCanSteal);

	AttachToActor(Cast<AActor>(PlayerGrabbing), FAttachmentTransformRules::KeepWorldTransform);

	OnBallGrabbed.Broadcast();
}

void ABall::MulticastGrabBall_Implementation(ALab_BlockoutStadiumCharacter* PlayerGrabbing, bool bCanSteal)
{
	BallMesh->SetSimulatePhysics(false);
	BallMesh->SetCollisionProfileName(FName("OverlapAll"));
}


void ABall::ReleaseBall(FVector ReleaseForce)
{
	ServerReleaseBall(ReleaseForce);
}

void ABall::ServerReleaseBall_Implementation(FVector ReleaseForce)
{
	bIsGrabbed = false;
	bCanBeStolen = true;

	if (GetAttachParentActor())
	{
		DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	}

	MulticastReleaseBall(ReleaseForce);

	//UE_LOG(LogTemp, Warning, TEXT("Ball - Ball Released"));
}

void ABall::MulticastReleaseBall_Implementation(FVector ReleaseForce)
{
	BallMesh->SetCollisionProfileName(FName("Ball"));
	BallMesh->SetSimulatePhysics(true);
	BallMesh->AddImpulse(ReleaseForce, NAME_None, true);
}


void ABall::HighlightBall(bool bIsHighlighted)
{
	ClientHighlightBall(bIsHighlighted);
}

void ABall::ClientHighlightBall_Implementation(bool bIsHighlighted)
{
	if (bIsHighlighted)
	{
		BallMesh->SetCustomDepthStencilValue(2);
	}
	else
	{
		BallMesh->SetCustomDepthStencilValue(1);
	}
}


void ABall::ServerChangeColour_Implementation(EPlayerTeam Team)
{
	MulticastChangeColour(Team);
}

void ABall::MulticastChangeColour_Implementation(EPlayerTeam Team)
{
	if (!MaterialGlassInstance)
		return;
	
	switch (Team)
	{
	case EPlayerTeam::TEAM_NONE:
		MaterialGlassInstance->SetVectorParameterValue("Colour", GameState->TeamNoneColour);
		break;

	case EPlayerTeam::TEAM1:
		MaterialGlassInstance->SetVectorParameterValue("Colour", GameState->TeamOneColour);
		break;

	case EPlayerTeam::TEAM2:
		MaterialGlassInstance->SetVectorParameterValue("Colour", GameState->TeamTwoColour);
		break;

	default:
		break;
	}
}


void ABall::ChangeState(float BallStateValue)
{
	ServerChangeState(BallStateValue);
}

void ABall::ServerChangeState_Implementation(float BallStateValue)
{
	MulticastChangeState(BallStateValue);
}


void ABall::MulticastChangeState_Implementation(float BallStateValue)
{
	if (!MaterialSubstanceInstance)
		return;

	MaterialSubstanceInstance->SetScalarParameterValue("BallState", BallStateValue);
}
